/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef PHYSOBJ_H
#define PHYSOBJ_H
#include "OgreVector3.h"
#include "btBulletDynamicsCommon.h"

using namespace Ogre;

enum { COLSHAPE_BOX, COLSHAPE_SPHERE, COLSHAPE_CYLINDER, COLSHAPE_CONVEX, COLSHAPE_TRIMESH, COLSHAPE_EMPTY };

class Obj;

class PhysObj
{
protected:
    btScalar mass;
    btScalar friction;
    btVector3 inertia;
public:
    int shapeType;
    int colType;
    int colWith;
    bool isWheel;
    btRigidBody* body;
    btCollisionShape* shape;
    void setMass(float _mass) { mass = _mass; }
    void setFriction(float val) { friction = val; }
    void setShapeType(int type_id) { shapeType = type_id; }
    void setLinearVelocity(Vector3 v);
    Vector3 getRelAngularVelocity();
    Vector3 getRelLinearVelocity();
    void setPos(Vector3 pos);
    void setOri(Quaternion ori);
    void setStatic();
    void setNoResponse();
    void rotateToAngle(float angle);
    void applyForce(Vector3 force, Vector3 rel_pos);
    void clearForces();
    void disableDeactivation();
    bool rayTest(Vector3 start, Vector3 end);
    void translate(Vector3 v);
    void spawn(void *_node, Entity *en);

    PhysObj(Obj *obj);
    virtual ~PhysObj();
    bool load(std::string fn);

    Obj *_obj; // link to gameobject
};

#endif
