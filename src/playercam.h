/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef PLAYERCAM_H
#define PLAYERCAM_H
#include "Ogre.h"
#include "OIS.h"
#include "SdkCameraMan.h"
#include "obj.h"
#include "globals.h"

using namespace Ogre;

class PlayerCam
{
public:
    PlayerCam(std::string name);
    void setAutoTracking(Obj *obj);
    void setCamType(int type);
    void update(float dt);

// protected
    OgreBites::SdkCameraMan* mCameraMan;
    Camera* mCamera;
    SceneNode *mTargetNode;
    Vector3 mTargetNodePrevPos;
    int mCamType;
};

#endif
