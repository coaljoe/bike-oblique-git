/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <iostream>
#include <Ogre.h>
#include "app.h"
#include "input.h"

using namespace std;

int main(int argc, char **argv) {
    cout << "main...\n";

    KL *kl1 = new KL();

    App *app = new App;
    //app->run();

    app->setup();
    app->buildScene();

    app->test();

    kl1->reg();
    while(1) {
        if(!app->step())
            break;
    }

    return 0;
}
