/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <iostream>
#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "obj.h"
#include "physics.h"
#include "event.h"
#include "globals.h"

using namespace std;
using namespace physics;

namespace physics
{

BtOgre::DebugDrawer *dbgdraw;
btAlignedObjectArray<btCollisionShape*> m_collisionShapes;
btBroadphaseInterface*  m_broadphase;
btCollisionDispatcher*  m_dispatcher;
btConstraintSolver*     m_solver;
btDefaultCollisionConfiguration* m_collisionConfiguration;
btDynamicsWorld*        m_dynamicsWorld;
btCollisionWorld*	m_collisionWorld;
bool debug = false;
float mSimulationSpeed = 1.0;
unsigned long physt, physt_prev;
Timer mTimer;

void init()
{
    printf("physics::init()\n");
    mTimer = Timer();
    physt = physt_prev = mTimer.getMilliseconds();
    ///collision configuration contains default setup for memory, collision setup
    m_collisionConfiguration = new btDefaultCollisionConfiguration();
    //m_collisionConfiguration->setConvexConvexMultipointIterations();

    ///use the default collision dispatcher.
    //For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

    m_broadphase = new btDbvtBroadphase();

    ///the default constraint solver.
    //For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
    m_solver = sol;

    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);

    m_dynamicsWorld->getDispatchInfo().m_enableSPU = false;
    m_dynamicsWorld->setGravity(btVector3(0,-10,0));

    btContactSolverInfo& info = m_dynamicsWorld->getSolverInfo();
    info.m_erp = 0.2;
    //info.m_erp2 = .9;
    //info.m_linearSlop = 0; //0.0001;
    info.m_linearSlop = 0.0001;
    info.m_numIterations = 10; // 20
    //info.m_splitImpulse = 1;
    //info.m_splitImpulsePenetrationThreshold = -0.02;
    info.m_solverMode |= SOLVER_USE_2_FRICTION_DIRECTIONS;

    /*
    ///create a few basic rigid bodies
    btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(50.),btScalar(50.),btScalar(50.)));
    //      btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0,1,0),50);

    m_collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();
    groundTransform.setOrigin(btVector3(0,-50,0));
    */

    m_collisionWorld = new btCollisionWorld(m_dispatcher, m_broadphase, m_collisionConfiguration);

    // debug
    dbgdraw = new BtOgre::DebugDrawer(globals::sm->getRootSceneNode(), m_dynamicsWorld);
    m_dynamicsWorld->setDebugDrawer(dbgdraw);
    dbgdraw->setDebugMode(physics::debug);
}

void deinit()
{
    //cleanup in the reverse order of creation/initialization

    //remove the rigidbodies from the dynamics world and delete them
    int i;
    for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
    {
            btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
            btRigidBody* body = btRigidBody::upcast(obj);
            if (body && body->getMotionState())
            {
                    delete body->getMotionState();
            }
            m_dynamicsWorld->removeCollisionObject( obj );
            delete obj;
    }

    //delete collision shapes
    for (int j=0;j<m_collisionShapes.size();j++)
    {
            btCollisionShape* shape = m_collisionShapes[j];
            delete shape;
    }

    delete m_dynamicsWorld;
    delete m_solver;
    delete m_broadphase;
    delete m_dispatcher;
    delete m_collisionConfiguration;
    delete m_collisionWorld;
    
    delete dbgdraw;
}


void update(float dt)
{
    physt = mTimer.getMilliseconds();
    float physdt = ((float)(physt - physt_prev))/1000.0;
    //m_dynamicsWorld->stepSimulation(physdt / 100.f);
    //m_dynamicsWorld->stepSimulation(physdt, 7);
    m_dynamicsWorld->stepSimulation(physdt * mSimulationSpeed, 10);
    //m_dynamicsWorld->stepSimulation(physdt, 20, 1./120.);
    //m_dynamicsWorld->stepSimulation(physdt, 40);
    physt_prev = physt;

    m_collisionWorld->performDiscreteCollisionDetection();

    int numManifolds = m_collisionWorld->getDispatcher()->getNumManifolds();
    int i;
    for(i=0;i<numManifolds;i++)
    {
        btPersistentManifold* pm = m_collisionWorld->getDispatcher()->getManifoldByIndexInternal(i);
        if(pm->getNumContacts() > 0)
        {
            const btCollisionObject* obA = pm->getBody0();
            const btCollisionObject* obB = pm->getBody1();

            //if(obA->isStaticObject() || obB->isStaticObject())
            //    ...

            PhysObj* phyA = (PhysObj*) obA->getUserPointer();
            PhysObj* phyB = (PhysObj*) obB->getUserPointer();

            if(!phyA || !phyB)
            {
                // collision with an object without the userpointer
            }
            else
            {
                // skip collision callbacks between walls and wheels
                //if(phyA->isWheel && (phyB->colType == COL_WALL))
                //    continue;
                // don't handle collisions with walls
                if(phyA->colType != COL_WALL && phyB->colType != COL_WALL)
                {
           
                    printf("Collision between: %d, %d.\n", phyA->_obj->id, phyB->_obj->id);
                    cout << phyA->_obj->m_name << " "
                         << phyB->_obj->m_name << endl;
                    
                    Event::send_event("ev_collide", phyA->_obj->m_name + " " + 
                                                    phyB->_obj->m_name);
                }
            }
        }

        //you can un-comment out this line, and then all points are removed
        pm->clearManifold();	
    }

    dbgdraw->step();
}

void setDebug(bool v)
{
    dbgdraw->setDebugMode(v);
    physics::debug = v;
}

void addFloor()
{
    btCollisionShape* floorShape = new btBoxShape(btVector3(50, 0, 50));
    m_collisionShapes.push_back(floorShape);

    btTransform floorTransform;
    floorTransform.setIdentity();
    floorTransform.setOrigin(btVector3(0, 0, 0));

    btScalar mass = 0.0;
    btVector3 inertia = btVector3(0, 0, 0);

    btDefaultMotionState* mots = new btDefaultMotionState(floorTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, mots, floorShape, inertia);
    btRigidBody *body = new btRigidBody(rbInfo);
    m_dynamicsWorld->addRigidBody(body);
}

Hinge::Hinge(Obj *ob1, Obj *ob2, Quaternion basisA, Vector3 originA,
                                 Quaternion basisB, Vector3 originB,
                                 bool disableCollision)
    : mFriction(0)
{
    btTransform localA, localB;
    localA.setIdentity();
    localB.setIdentity();
    /*
    localA.getBasis().setEulerZYX(0, 0, M_PI_2);
    localA.setOrigin(btVector3(0.0, 0.0, 0.0));
    localB.getBasis().setEulerZYX(0, 0, M_PI_2);
    localB.setOrigin(btVector3(0.0, 0.0, 0.0));
    */
    localB.getBasis().setRotation(BtOgre::Convert::toBullet(basisA));
    localA.setOrigin(BtOgre::Convert::toBullet(originA));
    localA.getBasis().setRotation(BtOgre::Convert::toBullet(basisB));
    localB.setOrigin(BtOgre::Convert::toBullet(originB));

    hinge = new btHingeConstraint(*ob1->phys->body,*ob2->phys->body, localA, localB);
    //ob1->phys.setConstraint(hinge);
    //ob2->phys.setConstraint(hinge);
    m_dynamicsWorld->addConstraint(hinge, disableCollision); // should be last

    //hinge->setLimit(1.0, -1.0, 1, 1, 0);
    //hinge->setAxis(&BtOgre::Convert::toBullet(Ogre::Vector3(0, 0, 1)));

    cout << "addHinge with " << ob1 << " " << ob2 << " "
                             << basisA << " " << originA << " "
                             << basisB << " " << originB << endl;
}

void Hinge::lock(bool lock, float softness, float bias)
{
    //btScalar _softness = 0.9;
    //btScalar _bias = 0.1;
    btScalar _softness = softness;
    btScalar _bias = bias;

    if(lock)
        hinge->setLimit(0, 0, _softness, _bias);
    else
        hinge->setLimit(1.0, -1.0, _softness, _bias);
}


void Hinge::rotateAngular(float vel, float impulse)
{
    //float l = 1745;
    //hinge->setLimit(-l, l);
    hinge->enableAngularMotor(true, vel, impulse);
}

void Hinge::rotateToAngle(float angle, float dt)
{
    hinge->enableMotor(true); 
    hinge->setMaxMotorImpulse(0.5); // power
    float _angle = hinge->getHingeAngle() + (angle * dt); // velocity
    hinge->setMotorTarget((btScalar)_angle, (btScalar)dt);
    //hinge->setAngularOnly(true);
}

void Hinge::setFriction(float val)
{
    mFriction = val;
    //use zero targetVelocity and a small maxMotorImpulse to simulate joint friction
    float targetVelocity = 0.f;
    float maxMotorImpulse = mFriction;
    hinge->enableAngularMotor(true,targetVelocity,maxMotorImpulse);
}

float Hinge::getMotorTargetVelosity()
{
    return hinge->getMotorTargetVelosity();
}

void Hinge::enableMotor(bool val)
{
    hinge->enableMotor(val);
    if(val == false &&  mFriction > 0)
        setFriction(mFriction);
}

float Hinge::getHingeAngle()
{
    return (float)hinge->getHingeAngle();
}

void Hinge::setAxis(Vector3 axis)
{
    btVector3 v;
    v = BtOgre::Convert::toBullet(axis);
    hinge->setAxis(v);
}

} // ::physics 

