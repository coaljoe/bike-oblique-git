/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef EVENT_H
#define EVENT_H
#include <string>
#include <zmq.hpp>

class Event
{
    static Event* m_pInstance;
    zmq::context_t ctx;
    zmq::socket_t socket;
public:
    static Event* Instance();

    Event();
    static bool send_event(std::string ev_name, std::string data);
    bool init();
    void update();
};


#endif
