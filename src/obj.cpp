/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <iostream>
#include "Ogre.h"
#include "jsoncpp/json/json.h"
#include "globals.h"
#include "obj.h"
#include "util.h"

using namespace std;
using namespace Ogre;

int _max_id = 0;

Obj::Obj() :
    id(0),
    spawned(false),
    m_pos(Vector3::ZERO),
    m_ori(Quaternion::IDENTITY),
    m_fn(""), m_materialName(""), m_meshFile(""), m_name("")
{
    // default object's state initialization
    phys = new PhysObj(this);

    id = _max_id;
    _max_id++;
}

Obj::~Obj()
{
    delete phys;
    globals::sm->destroySceneNode(mPivot);
}

bool Obj::load(string fn)
{
    cout << "Obj::load\n";
    std::ifstream t(fn.c_str());
    std::string buff((std::istreambuf_iterator<char>(t)),
                      std::istreambuf_iterator<char>());
    t.close();


    Json::Value root;   
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(buff, root);
    if(!parsingSuccessful)
    {
        std::cout  << "Obj::load - Failed to parse configuration\n"
                   << "fn = " << fn << endl
                   << reader.getFormattedErrorMessages();
        exit(-1);
    }

    m_meshFile = root["mesh"].asString();
    m_materialName = root["material"].asString();

    cout << "XXX\n" << m_meshFile << endl << m_materialName << endl;

    m_fn = fn;

    cout << "Obj::load ok\n";
    return true;
}

void Obj::setMesh(std::string s)
{
    m_meshFile = s;
}

void Obj::setMaterial(std::string s)
{
    m_materialName = s;
}

void Obj::rotate(Vector3 axis, Radian angle, bool nophys)
{

    Quaternion q = Quaternion(angle, axis);
    mPivot->rotate(q);
    m_ori = mPivot->getOrientation();
    if(!nophys)
        phys->setOri(m_ori);
    

#if 0
    //Node::TransformSpace ts = Node::TS_WORLD;
    //mPivot->rotate(Vector3::UNIT_Z, Degree(90), ts);
    //mPivot->rotate(axis, angle);
    Quaternion q = mPivot->getOrientation();
    cout << "rotate: q " << q << endl;
    q.FromAngleAxis(angle, axis);

    Quaternion q2;
    q2 = m_ori * q;
    m_ori = q2;
    //cout << "rotate: new_ori " << new_ori << endl;
    cout << "rotate: q " << q2 << endl;
    //mPivot->rotate(m_ori);
    phys->setOri(q2);
    cout << "rotate: q " << q2 << endl;
    cout << endl;
    //m_ori = mPivot->getOrientation();
    //cout << "rotate: m_ori " << q << endl;
#endif
}



void Obj::resetOriPos()
{
    m_pos = Vector3::ZERO;
    m_ori = Quaternion::IDENTITY;
}

void Obj::respawn()
{
    cout << "respawn" << endl;
    if(spawned) {
        delete phys;
        destroySceneNode(mPivot);
        // reset
        phys = new PhysObj(this);
        spawned = false;
    }
    spawn();
}

void Obj::spawn()
{
    if(spawned) {
        cout << "Already spawned: " << this << " " << m_name << " " << m_meshFile << endl;
        exit(-1);
    }

    cout << m_name << endl;

    // gfx
    SceneManager *sm = globals::sm;
    mPivot = sm->getRootSceneNode()->createChildSceneNode();

    SceneNode* node = mPivot->createChildSceneNode();
    mPivot->rotate(m_ori);
    mPivot->setPosition(m_pos);

    cout << "Spawned at " << mPivot->getPosition() << " " << mPivot->getOrientation() << endl;
    cout << "XXX2" << m_meshFile << endl << m_materialName << endl;

    mEnt = sm->createEntity(m_name, m_meshFile);
    //en->setCastShadows(true);
    //en->getMesh()->buildEdgeList();
    node->attachObject(mEnt);

    if(m_materialName.length() > 0) {
        mEnt->setMaterialName(m_materialName);
    }

    // phys
    if(m_fn.length() > 0)
        phys->load(m_fn);
    phys->spawn(mPivot, mEnt);
    
    spawned = true;
    
    //world::add_obj(this);
}
