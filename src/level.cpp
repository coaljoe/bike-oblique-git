/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <iostream>
#include "Ogre.h"
#include "globals.h"
#include "DotSceneLoader.h"
#include "level.h"

using namespace std;
using namespace Ogre;

Level::Level()
{
    m_levelName = "NOT_SET";
}

void Level::spawn()
{
    // gfx
    SceneManager *sm = globals::sm;
    mPivot = sm->getRootSceneNode()->createChildSceneNode();

    SceneNode* node = mPivot->createChildSceneNode();

    printf("Loading level '%s'...\n", m_levelName);
    DotSceneLoader* loader = new DotSceneLoader();
    //loader->parseDotScene("levels/test_level/scene.xml", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, sm, mPivot);
    loader->parseDotScene("scene.xml", \
            Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, sm);
    //dotscene = DotScene('scene.xml'), sm, node);
}

void Level::addStaticObj(string meshName)
{
    /*
    SceneNode* node = mPivot->createChildSceneNode();
    Entity* en = globals::sm->createEntity(tmpnam(NULL), meshName);
    */
}

void Level::addObj(Obj *obj)
{
}
