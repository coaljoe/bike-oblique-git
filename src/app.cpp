/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <cstdio>
#include <iostream>
#include <clocale>
#include <Ogre.h>
#include <jsoncpp/json/json.h>
#include "app.h"
#include "physics.h"
#include "obj.h"
#include "event.h"
#include "input.h"
#include "globals.h"
#include "audio.h"

using namespace std;
using namespace Ogre;

App::App() :
    mPause(false),
    mNoSound(false)
{
    setlocale(LC_ALL, "C");
    Event::Instance()->init();
}

App::~App()
{
    cout << "~App..." << endl;
    physics::deinit();
    mInputManager->destroyInputObject(mKeyboard); mKeyboard = 0;
    OIS::InputManager::destroyInputSystem(mInputManager); mInputManager = 0;
    delete mRoot;
}

void App::togglePhysDebug()
{
    physics::setDebug(!physics::debug);
}

bool App::step()
{
    Ogre::WindowEventUtilities::messagePump();
    if(mPause) {
        mKeyboard->capture();
        //if(mKeyboard->isKeyDown(OIS::KC_ESCAPE))
        //    return false;
        return true;
    }
    if(!mRoot->renderOneFrame()) {
        delete this;
        return false;
    }
    //cout << mWindow->getLastFPS() << endl;
    return true;
}

void App::pause(bool v)
{
    mPause = v;
    cout << "app:pause: " << mPause << endl;
    if(mPause) {
        //Viewport *vp = mRoot->getAutoCreatedWindow()->getViewport(0);
        //vp->setBackgroundColour(ColourValue(0.0, 1.0, 0.0));
    }
    else {

    }
}

bool App::keyReleased(const OIS::KeyEvent &e)
{
    std::vector<KL*>::iterator it;
    for(it=kls.begin(); it != kls.end(); it++)
    {
        //cout << "woot up!" << endl;
        KL *kl = *it;
        kl->keyrelease(e.key);
    }

    mCameraMan->injectKeyDown(e);
    mCameraMan->injectKeyUp(e); 
    return true;
}

bool App::keyPressed(const OIS::KeyEvent &e)
{
    switch (e.key)
    {
        //case OIS::KC_ESCAPE: 
        //    return false;
        //case OIS::KC_Q: 
        //    return false;
        case OIS::KC_1:
            std::cout << "111111111\n";
            break;
        default:
            break;
    }

    std::vector<KL*>::iterator it;
    for(it=kls.begin(); it != kls.end(); it++)
    {
        //cout << "woot!" << endl;
        KL *kl = *it;
        kl->keypress(e.key);
    }

    mCameraMan->injectKeyDown(e);
    return true;
}

bool App::frameRenderingQueued(const FrameEvent &evt)
{
    if(mWindow->isClosed())
        return false;
    if(mPause)
        return true;
    mKeyboard->capture();

    //if(mKeyboard->isKeyDown(OIS::KC_ESCAPE))
    //    return false;


    float dt = (float)evt.timeSinceLastFrame;
    physics::update(dt);
    mPlayerCam->update(dt);
    if(!mNoSound)
        audio::update(dt);
    globals::dt = dt;
    //cout << "dt" << evt.timeSinceLastFrame << endl;

    return true;
}

void App::buildScene(void)
{
    /*
    BikeWheelModel *bwm1 = new BikeWheelModel(Vector3(-5, -5, -5));
    BikeWheelView *bw1 = new BikeWheelView();
    bw1->set_model(bwm1);
    bw1->spawn(mSceneMgr);
    bw1->update();
    */

    physics::addFloor();

    Obj *fwheel = new Obj();
    fwheel->setPos(Vector3(-3, 0.5, 0));
    fwheel->setMesh("objects/wheel.mesh");
    fwheel->phys->setMass(1.5);
    fwheel->phys->shapeType = COLSHAPE_CONVEX;
    fwheel->setMaterial("Examples/Chrome");
    //fwheel->rotate(Vector3::UNIT_Z, Degree(90));
    printf("WOOT\n");
    fwheel->spawn();

    Obj *fork = new Obj();
    fork->setPos(Vector3(-3, 0.5, 0));
    fork->setMesh("objects/fork.mesh");
    fork->phys->setMass(2.5);
    fork->phys->shapeType = COLSHAPE_CONVEX;
    fork->setMaterial("Examples/RustySteel");
    printf("WOOT\n");
    fork->spawn(); // sm-less spawn

    {
        Quaternion basis;
        basis.FromAngleAxis(Degree(90), Vector3::UNIT_Z);
        Vector3 origin = Vector3(0, 0, 0);
        physics::Hinge(fork, fwheel, basis, origin, basis, origin);
    }

    Obj *rwheel = new Obj();
    rwheel->setPos(Vector3(-3 - 1.0, 0.5, 0));
    rwheel->setMesh("objects/wheel.mesh");
    rwheel->phys->setMass(1.5);
    rwheel->phys->shapeType = COLSHAPE_CONVEX;
    rwheel->setMaterial("Examples/Chrome");
    //rwheel->rotate(Vector3::UNIT_Z, Degree(90));
    rwheel->spawn();

    Obj *frame = new Obj();
    frame->setPos(Vector3(-3 + -0.520, 0.4, 0));
    frame->setMesh("objects/frame.mesh");
    frame->phys->setMass(10);
    frame->phys->shapeType = COLSHAPE_CONVEX;
    frame->setMaterial("Examples/RustySteel");
    frame->spawn();

    {
        Quaternion basis;
        basis.FromAngleAxis(Degree(90), Vector3::UNIT_Z);
        Vector3 originA = Vector3(-0.480, 0, 0);
        Vector3 originB = Vector3(0, 0, 0);
        physics::Hinge(frame, rwheel, basis, originA, basis, originB);
    }

    {
        // connect frame with fork
        Quaternion basisA, basisB;
        basisA.FromAngleAxis(Degree(90), Vector3::UNIT_X);
        basisB.FromAngleAxis(Degree(90), Vector3::UNIT_X);
        Vector3 originA = Vector3(0.5, 0.5, 0); // connection point on frame
        Vector3 originB = Vector3(0, 0.5, 0);   // connection point on fork
        physics::Hinge h = physics::Hinge(frame, fork, basisA, originA, basisA, originB);
        h.rotateToAngle(-0.58, 0.1);
    }

    //frame->phys->setLinearVelocity(Vector3(7.0, 0, 0));
    //frame->phys->setTorque(10);


    Obj *box = new Obj();
    box->setPos(Vector3(1.5, -0.95, 0));
    box->setMesh("objects/box.mesh");
    box->setMaterial("Examples/RustySteel");
    box->spawn();

    Obj *wheel2 = new Obj();
    wheel2->setPos(Vector3(1.5, 2.5, 0));
    wheel2->setMesh("objects/wheel.mesh");
    wheel2->phys->setMass(0);
    wheel2->phys->shapeType = COLSHAPE_CYLINDER;
    wheel2->setMaterial("Examples/Chrome");
    wheel2->spawn();
}

void App::addFloor() { physics::addFloor(); }

void App::test()
{
    Plane plane;
    plane.normal = Vector3::UNIT_Y;
    plane.d = 0;
    MeshManager::getSingleton().createPlane("Myplane",
            ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
            500,500,25,25,true,1,180,180,Vector3::UNIT_Z);

    Entity* pPlaneEnt = mSceneMgr->createEntity( "plane", "Myplane" );
    pPlaneEnt->setMaterialName("greensnow");
    pPlaneEnt->setCastShadows(false);
    mSceneMgr->getRootSceneNode()->createChildSceneNode(Vector3(0,0,0))->attachObject(pPlaneEnt);

}

void App::addResLoc(string loc, string grpname)
{
    cout << "addResLoc: " << loc << "... " << endl;
    ResourceGroupManager &mgr = ResourceGroupManager::getSingleton();
    mgr.addResourceLocation(loc, "FileSystem", grpname, true);
    mgr.initialiseAllResourceGroups();
}

void App::removeResGroup(string grpname)
{
    cout << "removeResGroup: " << grpname << "... " << endl;
    ResourceGroupManager &mgr = ResourceGroupManager::getSingleton();
    if(mgr.resourceGroupExists(grpname)) {
        mgr.unloadResourceGroup(grpname);
        mgr.destroyResourceGroup(grpname);
        mgr.initialiseAllResourceGroups();
    }
}

bool App::loadLeveljson(string fn)
{
    // load scene and other definition from level.json
    cout << "App::loadLeveljson\n";
    std::ifstream t(fn.c_str());
    std::string buff((std::istreambuf_iterator<char>(t)),
                      std::istreambuf_iterator<char>());
    t.close();

    Json::Value root, nd, nd1;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(buff, root);
    if(!parsingSuccessful)
    {
        std::cout  << "Failed to parse configuration\n" << "fn = " << fn << endl
                   << reader.getFormattedErrorMessages();
        exit(-1);
    }

// unpack node
#define ND2VEC(nd) nd[0].asFloat(), nd[1].asFloat(), nd[2].asFloat()

    // load sky
    nd1 = root["scene"]["sky"];
    string type = nd1["type"].asString();
    if(!nd1.empty()) {
        // redefine sky
        mSceneMgr->setSkyPlaneEnabled(false);
        mSceneMgr->setSkyBoxEnabled(false);
        mSceneMgr->setSkyDomeEnabled(false);
    }
    if(type == "skydome")
    {
        string matname = nd1["material"].asString();
        mSceneMgr->setSkyDome(true, matname, 2, 5, 10);
        nd = nd1["params"]["scroll"];
        if(!nd.empty()) {
            MaterialPtr mat = MaterialManager::getSingleton().getByName(matname);
            mat->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(
                    nd[0].asFloat(), nd[1].asFloat());
        }
    }
    else if(type == "skybox") {
        mSceneMgr->setSkyBox(true, nd1["material"].asString(), 10);
    }
    else if(type == "skyplane") {
        Ogre::Plane plane;
        plane.d = 500;
        plane.normal = Ogre::Vector3::NEGATIVE_UNIT_Y;
        float bow = nd1.get("bow", 0.0).asFloat();
        mSceneMgr->setSkyPlane(true, plane, nd1["material"].asString(), 500, 10, true, bow, 10, 10);
    }
    else if(type == "none") {
        mSceneMgr->setSkyDomeEnabled(false);
    }

    // load lights
    for(int i=1; i < 3; i++) {
        string lname = "light" + StringConverter::toString(i);
        nd1 = root["scene"][lname];
        if(!nd1.empty())
        {
            mSceneMgr->destroyLight(lname);
            Light* l = mSceneMgr->createLight(lname);
            if(!(nd = nd1["cast_shadows"]).empty()) {
                l->setCastShadows(nd.asBool());
            }
            if(!(nd = nd1["diffuse_color"]).empty()) {
                l->setDiffuseColour(ND2VEC(nd));
            }
            if(!(nd = nd1["position"]).empty()) {
                l->setPosition(ND2VEC(nd));
            }
            if(!(nd = nd1["direction"]).empty()) {
                l->setDirection(ND2VEC(nd));
            }
            if(!(nd = nd1["visible"]).empty()) {
                l->setVisible(nd.asBool());
            }
            if(!(nd = nd1["type"]).empty()) {
                string s = nd.asString();
                if(s == "point") l->setType(Light::LT_POINT);
                else if(s == "directional") l->setType(Light::LT_DIRECTIONAL);
                else {
                    cout << "error: uknown light type: " << s << endl;
                    exit(-1);
                }
            }
        }
    }

    // ambient light
    if(!(nd = root["scene"]["ambient_light"]).empty()) {
        mSceneMgr->setAmbientLight(ColourValue(ND2VEC(nd)));
    }

    cout << "App::loadLeveljson ok\n";
    return true;
}

void App::resetScene()
{
    // fixme

    mSceneMgr->setSkyPlaneEnabled(false);
    mSceneMgr->setSkyBoxEnabled(false);
    mSceneMgr->setSkyDomeEnabled(false);

    // light
    mSceneMgr->destroyLight("light1");
    mSceneMgr->destroyLight("light2");
}

void App::setDefaultScene()
{
    float v = 0.5225;
    mSceneMgr->setAmbientLight(ColourValue(v, v, v));

    // sky
    /*
    Ogre::Plane plane;
    plane.d = 500;
    plane.normal = Ogre::Vector3::NEGATIVE_UNIT_Y;
    mSceneMgr->setSkyPlane(true, plane, "Examples/SpaceSkyPlane", 500, 10, true, .5, 10, 10);
    */

    // default sky
    mSceneMgr->setSkyDome(true, "Examples/CloudySky", 2, 5, 10);
    MaterialPtr mat = MaterialManager::getSingleton().getByName("Examples/CloudySky");
    mat->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setScrollAnimation(0.01, 0);
    //mSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox", 10);
    //mSceneMgr->setSkyBox(true, "Examples/EarlyMorningSkyBox", 10);

    /*
    Light* l1 = mSceneMgr->createLight("ligth1");
    l1->setType(Light::LT_POINT);
    l1->setPosition(Vector3(20, 30, -20));
    l1->setDiffuseColour(.9, .9, .8);
    //l1->setDiffuseColour(.2, .2, .2);
    l1->setCastShadows(false);

    Light* l2 = mSceneMgr->createLight("light2");
    l2->setType(Light::LT_DIRECTIONAL);
    l2->setDirection(Vector3(15, -60, -45));
    //l2->setPosition(Vector3(-10,-10,100));
    //l2->setDirection(Vector3(2,2,-10));
    l2->setDiffuseColour(.45, .62, .6);
    l2->setCastShadows(true);
    */

    // default light
    Light* l1 = mSceneMgr->createLight("light1");
    l1->setType(Light::LT_POINT);
    l1->setPosition(Vector3(50, 300, -50));
    //l1->setDiffuseColour(.9, .9, .8);
    l1->setDiffuseColour(.2, .2, .2);
    l1->setCastShadows(false);
    l1->setVisible(false);

    Light* l2 = mSceneMgr->createLight("light2");
    l2->setType(Light::LT_DIRECTIONAL);
    l2->setDirection(Vector3(15, -20, -45));
    //l2->setPosition(Vector3(-10,-10,100));
    //l2->setDirection(Vector3(2,2,-10));
    l2->setDiffuseColour(.45, .62, .6);
    l2->setCastShadows(true);
}

bool App::setup()
{
    // Enter Ogre
    mRoot = new Root;

    // Load resource paths
    ConfigFile cf;
    cf.load("resources.cfg");

    ConfigFile::SectionIterator seci = cf.getSectionIterator();

    String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap *settings = seci.getNext();
        ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            ResourceGroupManager::getSingleton().addResourceLocation(
                    archName, typeName, secName, false); // recursive
        }
    }

    // Configure
    if(!mRoot->restoreConfig())
        if(!mRoot->showConfigDialog())
            return false;
    mWindow = mRoot->initialise(true, "0.0.1 alpha");

    // Resources
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    // SceneManager
    globals::sm = mRoot->createSceneManager(ST_GENERIC, "SMInstance");
    mSceneMgr = globals::sm;

    // Camera
    mPlayerCam = new PlayerCam("PlayerCam");
    mCameraMan = new OgreBites::SdkCameraMan(mPlayerCam->mCamera);
    Viewport* viewPort = mWindow->addViewport(mPlayerCam->mCamera);

    // FrameListeners
    mRoot->addFrameListener(this);

    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    //tell OIS about the Ogre window
    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
    if(getenv("DEBUG")) {
        pl.insert(std::make_pair(std::string("x11_mouse_grab"), "false"));
        pl.insert(std::make_pair(std::string("x11_mouse_hide"), "false"));
        pl.insert(std::make_pair(std::string("x11_keyboard_grab"), "false"));
    }

#if defined OIS_WIN32_PLATFORM
    pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
    pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_EXCLUSIVE")));
#elif defined OIS_LINUX_PLATFORM
    pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
    pl.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("false")));
#endif

    mInputManager = OIS::InputManager::createInputSystem(pl);
    mKeyboard = static_cast<OIS::Keyboard*> \
               (mInputManager->createInputObject(OIS::OISKeyboard, true));
    mKeyboard->setEventCallback(this);

    // Misc
    mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);
    //mSceneMgr->setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE);
    //mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);
    //mSceneMgr->setShadowFarDistance(2000);
    MaterialManager *mm = MaterialManager::getSingletonPtr();
    mm->setDefaultAnisotropy(8); //16
    //MeshManager::getSingleton().setPrepareAllMeshesForShadowVolumes(true);


    // subsystems
    physics::init();

    if(!mNoSound)
        audio::init_sound();

    return true;
}

