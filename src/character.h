/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef CHARACTER_H
#define CHARACTER_H
#include "Ogre.h"

using namespace Ogre;

class btKinematicCharacterController;

class Character
{
protected:
    SceneNode* mPivot;
    char* m_characterName;
    btKinematicCharacterController* m_character;
    class   btPairCachingGhostObject* m_ghostObject;
public:
    // Methods
    Character();
    void load(const char *s);

    void spawn();
    void walk();
    void update(float dt);
};

#endif
