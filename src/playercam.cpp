/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <cstdio>
#include <iostream>
#include "Ogre.h"
#include "OIS.h"
#include "SdkCameraMan.h"
#include "playercam.h"
#include "obj.h"
#include "globals.h"

using namespace std;
using namespace Ogre;

PlayerCam::PlayerCam(string name) :
    mTargetNode(NULL),
    mCamType(NULL)
{
    // Camera
    mCamera = globals::sm->createCamera(name); 
    mCamera->setNearClipDistance(0.01);
    //mCamera->setFarClipDistance(500);
    mCamera->setAutoAspectRatio(true);

    SceneNode* camNode = globals::sm->getRootSceneNode()->createChildSceneNode("CamNode");
    camNode->attachObject(mCamera);

    // cabinet projection
     
    /*
    mCamera->setProjectionType(PT_ORTHOGRAPHIC);
    mCamera->setOrthoWindowHeight(10);

    mCamera->setPosition(0, 5, 3);
    //mCamera->lookAt(Vector3(0, 0, 0));

    Matrix4 m;
    m = mCamera->getProjectionMatrix();
    m[0][2]=-2*Math::Cos(Degree(45))/mCamera->getOrthoWindowWidth();
    m[1][2]=-2*Math::Sin(Degree(45))/mCamera->getOrthoWindowHeight();
    mCamera->setCustomProjectionMatrix(true,m);
    */

    /*
    // iso-cam
    mCamera->setProjectionType(PT_ORTHOGRAPHIC);
    mCamera->setOrthoWindowHeight(10);

    mCamera->setPosition(3, 4.5, 3);
    mCamera->lookAt(Vector3(0, 0, 0));

    //mCamera->setPosition(0, 20, 10);
    //mCamera->lookAt(Vector3(0, 0, 0));
    */

    // iso-cam2
    //mCamera->setProjectionType(PT_ORTHOGRAPHIC);
    //mCamera->setOrthoWindowWidth(13);

    //mCamera->setPosition(3, 0, 10);
    //mCamera->lookAt(Vector3(0, 0, 0));
    //mCamera->setPosition(-1.519, -20.506, 7.344);
    //mCamera->setPosition(-0.519, 7.344, 20.506);
    //mCamera->setDirection(79.816, -2.644, -14.750);
    //mCamera->setDirection(79.816, -14.750, -2.644);
    //mCamera->setDirection(0, 0, 80);
    
    //mCamera->pitch(Degree(-20));
    //mCamera->yaw(Degree(-10));

    /*
    mCamera->pitch(Degree(79.816 - 90.0));
    mCamera->roll(Degree(2.644));
    mCamera->yaw(Degree(-14.750)); // -10
    */

    //mCamera->lookAt(Vector3(0, 0, 0));


    setCamType(0);

    mCameraMan = new OgreBites::SdkCameraMan(mCamera);
    mCameraMan->setStyle(OgreBites::CS_MANUAL);
}

void PlayerCam::setCamType(int type)
{
    mCamera->setProjectionType(PT_PERSPECTIVE);
    mCamera->yaw(Degree(0));
    mCamera->pitch(Degree(0));
    mCamera->roll(Degree(0));
    mCamera->setDirection(Vector3(0, 0, -1)); // default direction
    mCamera->setFOVy(Degree(40));

    // next cam
    if(type == -1) {
        type = (mCamType + 1) % 4;
    }


    Vector3 pos = Vector3::ZERO;
    if(mTargetNode) {
        pos = mTargetNode->getPosition(); 
    }

    if(type == 0) {
        // iso-cam2
        mCamera->setProjectionType(PT_ORTHOGRAPHIC);
        mCamera->setOrthoWindowWidth(13);
        //mCamera->setPosition(-0.519, 7.344, 20.506);
        mCamera->setPosition(pos + Vector3(-3.1, 5.1, 20.506));
        mCamera->pitch(Degree(79.816 - 90.0));
        mCamera->roll(Degree(2.644));
        mCamera->yaw(Degree(-14.750)); // -10
    }
    else if(type == 1) {
        // iso-cam3 persp
        //mCamera->setPosition(pos + Vector3(-0.519, 7.344, 20.506));
        mCamera->setPosition(pos + Vector3(-3.1, 5.1, 20.506));
        mCamera->pitch(Degree(79.816 - 90.0));
        mCamera->roll(Degree(2.644));
        mCamera->yaw(Degree(-14.750)); // -10
    }
    else if(type == 2) {
        // iso-cam
        mCamera->setProjectionType(PT_ORTHOGRAPHIC);
        mCamera->setOrthoWindowHeight(10);
        //mCamera->setPosition(3, 4.5, 3);
        //mCamera->lookAt(Vector3(0, 0, 0));
        mCamera->setPosition(pos + Vector3(3, 4.5, 3));
        mCamera->lookAt(pos);
    }
    else if(type == 3) {
        // persp top cam
        mCamera->setPosition(pos + Vector3(2, 5, 30));
        mCamera->pitch(Degree(-10));
        mCamera->yaw(Degree(10));
        mCamera->setFOVy(Degree(25));
    }
    else {
        cout << "unknown camtype: " << type << endl;
        return;
    }

    cout << "cam Position: " << mCamera->getPosition() << endl;
    cout << "cam Direction: " << mCamera->getDirection() << endl;
    mCamType = type;
}

void PlayerCam::setAutoTracking(Obj *obj)
{
    //mCamera->setAutoTracking(true, obj->getSceneNode());
    mTargetNode = obj->getSceneNode();
    mTargetNodePrevPos = mTargetNode->getPosition(); 

    // reset cam
    setCamType(mCamType);
}

void PlayerCam::update(float dt)
{
    if(mTargetNode) {
        Vector3 cpos = mCamera->getPosition();
        Vector3 npos = mTargetNode->getPosition(); 
        Vector3 pos = Vector3(cpos + (npos - mTargetNodePrevPos));
        //mCamera->setPosition(Vector3(pos[0], npos[1] + 5, cpos[2]));
        mCamera->setPosition(Vector3(pos[0], pos[1], cpos[2]));
        mTargetNodePrevPos = mTargetNode->getPosition(); 
    }
}
