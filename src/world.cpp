/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <vector>
#include "obj.h"
#include "world.h"
#include "globals.h"

namespace world
{
std::vector<Obj*> m_objs;

void add_obj(Obj* o)
{
    m_objs.push_back(o);
}

void update(float dt)
{
    for(int i; i < m_objs.size(); i++) {
        m_objs[i]->update(dt);
    }

    globals::dt = dt;
}

} // ::world
