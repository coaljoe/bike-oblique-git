/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef INPUT_H
#define INPUT_H
#include <vector>

class KL
{
public:
    //void execute() { std::cout << "KL exec\n"; }
    //virtual void keypress(int kc) { std::cout << "KL keypress " << kc << std::endl; }
    virtual void keypress(int kc) {};
    virtual void keyrelease(int kc) {};
    void reg();
};

extern std::vector<KL*> kls;

#endif
