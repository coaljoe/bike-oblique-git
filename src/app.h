/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef APP_H
#define APP_H
#include <cstdio>
#include <iostream>
#include "Ogre.h"
#include "OIS.h"
#include "SdkCameraMan.h"
#include "playercam.h"

using namespace Ogre;

class App : public FrameListener, public OIS::KeyListener, public OIS::MouseListener
{
public:
    App();
    virtual ~App();

    void run() {
        try { setup(); mRoot->startRendering(); }
        catch(Exception& e) { fprintf(stderr, "An exception has occurred: %s\n", e.what()); }
    }

    bool setup();
    void setNoSound() { mNoSound = true; }
    void togglePhysDebug();

    bool step();
    void pause(bool v);

    void buildScene();
    void resetScene();
    void setDefaultScene();
    void test();
    void addResLoc(std::string loc, std::string grpname);
    void removeResGroup(std::string grpname);
    bool loadLeveljson(std::string fn);
    SceneManager* getSM() { return this->mSceneMgr; }
    //Physics getPhysics() { return physics; }
    void addFloor();
    float getdt() { return globals::dt; }
    PlayerCam* getPlayerCam() { return mPlayerCam; }

protected:
    Root* mRoot;
    Camera* mCamera;
    RenderWindow* mWindow;
    SceneManager* mSceneMgr;
    FrameListener* mFrameListener;
    OgreBites::SdkCameraMan* mCameraMan;
    PlayerCam* mPlayerCam;
    OIS::InputManager* mInputManager;
    OIS::Keyboard* mKeyboard;
    bool mPause;
    bool mNoSound;

    virtual bool frameRenderingQueued(const FrameEvent& evt);
    virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);
    virtual bool mouseMoved(const OIS::MouseEvent &arg) { return false; };
    virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) { return false; };
    virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) { return false; };
};

#endif
