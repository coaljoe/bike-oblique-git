/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <iostream>
#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "BulletDynamics/Character/btKinematicCharacterController.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "globals.h"
#include "physics.h"
#include "character.h"

using namespace std;
using namespace Ogre;

Character::Character()
{
    m_characterName = "NOT_SET";
}

void Character::spawn()
{
    // gfx
    SceneManager *sm = globals::sm;
    mPivot = sm->getRootSceneNode()->createChildSceneNode();

    SceneNode* node = mPivot->createChildSceneNode();

    printf("Spawning character '%s'...\n", m_characterName);

    // phys

    btTransform startTransform;
    startTransform.setIdentity ();
    startTransform.setOrigin (btVector3(0.0, 4.0, 0.0));
    //startTransform.setOrigin (btVector3(10.210098,-1.6433364,16.453260));

    m_ghostObject = new btPairCachingGhostObject();
    m_ghostObject->setWorldTransform(startTransform);
    btScalar characterHeight=1.75;
    btScalar characterWidth =1.75;
    btConvexShape* capsule = new btCapsuleShape(characterWidth,characterHeight);
    m_ghostObject->setCollisionShape (capsule);
    m_ghostObject->setCollisionFlags (btCollisionObject::CF_CHARACTER_OBJECT);

    btScalar stepHeight = btScalar(0.35);
    m_character = new btKinematicCharacterController (m_ghostObject,capsule,stepHeight);

    ///only collide with static for now (no interaction with dynamic objects)
    physics::m_dynamicsWorld->addCollisionObject(m_ghostObject,btBroadphaseProxy::CharacterFilter,
            btBroadphaseProxy::StaticFilter|btBroadphaseProxy::DefaultFilter);

    physics::m_dynamicsWorld->addAction(m_character);

    // reset
    m_character->reset();
}

void Character::update(float dt)
{
    btVector3 walkDirection = btVector3(0.0, 0.0, 0.0);
    btScalar walkVelocity = btScalar(1.1) * 4.0; // 4 km/h -> 1.1 m/s
    btScalar walkSpeed = walkVelocity * dt;

    m_character->setWalkDirection(walkDirection*walkSpeed);
}

