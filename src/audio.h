/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef AUDIO_H
#define AUDIO_H
#include <Ogre.h>
#include <OgreOggSound.h>
#include "obj.h"

using namespace Ogre;

namespace audio
{
extern OgreOggSound::OgreOggSoundManager *soundManager;

void init_sound();
void update(float dt);
}

class AudioObj
{
private:
    OgreOggSound::OgreOggISound *snd;
public:
    AudioObj(std::string name, std::string file, bool loop=false);
    void play();
    void stop() { snd->stop(); }
    void pause() { snd->pause(); }
    void attach(Obj *ob);
    void setPitch(float val);
    void setVolume(float val);
};

#endif
