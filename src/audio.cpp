/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <Ogre.h>
#include <OgreOggSound.h>
#include <al.h>
#include "audio.h"
#include "globals.h"

using namespace Ogre;
using namespace OgreOggSound;
using namespace std;

namespace audio
{
// static
OgreOggSoundManager *soundManager = NULL;
SceneNode *mEarNode;

void init_sound()
{
    cout << "init sound" << endl;
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    Ogre::Root::getSingleton().loadPlugin("OgreOggSound");
#else
    Ogre::Root::getSingleton().loadPlugin("libOgreOggSound");
#endif
    //new OgreOggSound::OgreOggSoundManager();
    //soundManager = new OgreOggSoundManager();
    soundManager = OgreOggSoundManager::getSingletonPtr();
    assert(soundManager != NULL);
    if(!soundManager->init()) {
        cout << "soundManager init failed." << endl;
        exit(-1);
    }
    if(!soundManager->createListener()) {
        cout << "XXX sound error: can get listener" << endl;
        exit(-1);
    }
    //soundManager->createSound("test", "sounds/pick1.ogg", false, false, true);
    //soundManager->getSound("test")->play();

    /*
    cout << "test" << endl;
    Camera *cam = globals::sm->getCamera("PlayerCam");
    mEarNode = globals::sm->getRootSceneNode()->createChildSceneNode();
    //cam->getParentSceneNode()->attachObject(soundManager->getListener());
    mEarNode->setPosition(cam->getPosition());
    mEarNode->setOrientation(cam->getOrientation());
    //mEarNode->attachObject(soundManager->getListener());
    */

    cout << "init sound ok" << endl;
}

void update(float dt)
{
    /*
    //cout << "update... " << endl;
    //cout << cam->getPosition() << endl;
    //mEarNode->setPosition(cam->getPosition());
    //mEarNode->setOrientation(cam->getOrientation());
    //soundManager->getListener()->setPosition(cam->getPosition());
    //soundManager->getListener()->setOrientation(cam->getOrientation());
    */

    Camera *cam = globals::sm->getCamera("PlayerCam");

    // temporary overwork
    Vector3 p = cam->getPosition();
    alListener3f(AL_POSITION,p.x, p.y, p.z);

    Quaternion q = cam->getOrientation();
    float mOrientation[6];
    Ogre::Vector3 vDirection = q.zAxis();
    Ogre::Vector3 vUp = q.yAxis();

    mOrientation[0] = -vDirection.x;
    mOrientation[1] = -vDirection.y;
    mOrientation[2] = -vDirection.z;
    mOrientation[3] = vUp.x;
    mOrientation[4] = vUp.y;
    mOrientation[5] = vUp.z;	
    alListenerfv(AL_ORIENTATION,mOrientation);	

    soundManager->update(dt);
}
} // ::audio

AudioObj::AudioObj(string name, string file, bool loop)
{
    if(audio::soundManager->getSound(name)) {
        snd = audio::soundManager->getSound(name);
        cout << "sound '" << name << "' re-created." << endl;
    }
    else {
        snd = audio::soundManager->createSound(name, file, false, loop, true);
        cout << "sound '" << name << "' created." << endl;
    }
}

void AudioObj::play()
{
    snd->play();
}

void AudioObj::attach(Obj *ob)
{
    /*
    cout << "lel" << endl;
    //snd->detachFromParent();
    cout << snd << endl;
    cout << ob->getSceneNode()->getPosition() << endl;
    snd->setVolume(0);
    cout << "test" <<endl;
    cout << snd->isAttached() << endl;
    if(snd->getParentSceneNode())
    */
    ob->getSceneNode()->attachObject(snd);
}

void AudioObj::setPitch(float val)
{
    snd->setPitch(val);
}

void AudioObj::setVolume(float val)
{
    snd->setVolume(val);
}

