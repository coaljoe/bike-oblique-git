/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "jsoncpp/json/json.h"
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"
#include "physics.h"
#include "physobj.h"

using namespace Ogre;
using namespace std;

PhysObj::PhysObj(Obj *obj)
{
    _obj = obj;
    isWheel = false;
    mass = 0.0f;
    friction = 1.0f;
    inertia = btVector3(0, 0, 0);
    shapeType = COLSHAPE_BOX;
    colType = COL_WALL;
    colWith = COL_WALL|COL_BIKE;
}

PhysObj::~PhysObj()
{
    physics::m_dynamicsWorld->removeRigidBody(body);
    physics::m_collisionShapes.remove(shape);
}

bool PhysObj::load(string fn)
{
    cout << "PhysObj::load\n";
    std::ifstream t(fn.c_str());
    std::string buff((std::istreambuf_iterator<char>(t)),
                      std::istreambuf_iterator<char>());
    t.close();

    Json::Value root;   
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(buff, root);
    if(!parsingSuccessful)
    {
        std::cout  << "PhysObj::load - Failed to parse configuration\n"
                   << "fn = " << fn << endl
                   << reader.getFormattedErrorMessages();
        exit(-1);
    }

    shapeType = root["phys"]["shapeType"].asInt();
    mass = root["phys"]["mass"].asFloat();
    friction = root["phys"]["friction"].asFloat();
    colType = root["phys"]["colType"].asInt();
    colWith = root["phys"]["colWith"].asInt();

    cout << " mass: " << mass << endl
         << " shapeType: " << shapeType << endl
         << " friction: " << friction << endl
         << " colType: " << colType << endl
         << " colWith: " << colWith << endl;

    cout << "PhysObj::load ok\n";
    return true;
}

void PhysObj::setPos(Vector3 pos)
{
    btVector3 _pos = BtOgre::Convert::toBullet(pos);
    
    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(_pos);
    body->setWorldTransform(trans);
}

void PhysObj::setOri(Quaternion ori)
{
    btQuaternion q = BtOgre::Convert::toBullet(ori);
    
    btTransform trans = body->getCenterOfMassTransform();
    trans.setRotation(q);
    body->setCenterOfMassTransform(trans);
}

void PhysObj::setLinearVelocity(Vector3 v)
{
    btVector3 _v = BtOgre::Convert::toBullet(v);
    body->setLinearVelocity(_v);
}

Vector3 PhysObj::getRelAngularVelocity()
{
    /*
    btTransform trans;
    body->getMotionState()->getWorldTransform(trans);
    //return BtOgre::Convert::toOgre(body->getAngularVelocity() * trans.getBasis());
    */
    return BtOgre::Convert::toOgre(body->getCenterOfMassTransform().getBasis().transpose() * body->getAngularVelocity());
}

Vector3 PhysObj::getRelLinearVelocity()
{
    return BtOgre::Convert::toOgre(body->getCenterOfMassTransform().getBasis().transpose() * body->getLinearVelocity());
}

void PhysObj::setStatic()
{
    // xxx: cannot be called before the object spawned
    body->setCollisionFlags(body->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);
    body->setActivationState(DISABLE_SIMULATION);
   
    // disable static-to-static potential collision (?)
    body->getBroadphaseProxy()->m_collisionFilterGroup = btBroadphaseProxy::StaticFilter;;
    body->getBroadphaseProxy()->m_collisionFilterMask = btBroadphaseProxy::AllFilter ^ btBroadphaseProxy::StaticFilter;
}

void PhysObj::setNoResponse()
{
    body->setCollisionFlags(body->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
}

void PhysObj::applyForce(Vector3 force, Vector3 rel_pos)
{
    // FIXME
    btVector3 _force = BtOgre::Convert::toBullet(force);
    btVector3 _rel_pos = BtOgre::Convert::toBullet(rel_pos);
    body->applyCentralImpulse(_force);
}

void PhysObj::clearForces()
{
    body->clearForces();
}

void PhysObj::disableDeactivation()
{
    body->setActivationState(DISABLE_DEACTIVATION);
}

bool PhysObj::rayTest(Vector3 start, Vector3 end)
{
    btVector3 _start = BtOgre::Convert::toBullet(start);
    btVector3 _end = BtOgre::Convert::toBullet(end);
     
    btCollisionWorld::ClosestRayResultCallback rayCallback(_start, _end);
    // exclude self
    rayCallback.m_collisionFilterGroup = body->getBroadphaseHandle()->m_collisionFilterGroup;
    rayCallback.m_collisionFilterMask = body->getBroadphaseHandle()->m_collisionFilterMask;
     
    // Perform raycast
    physics::m_dynamicsWorld->rayTest(_start, _end, rayCallback);
     
    if(rayCallback.hasHit()) {
        //cout << "yay" << endl;
        return true;
    }

    return false;
}

void PhysObj::translate(Vector3 v)
{
    btVector3 _v = BtOgre::Convert::toBullet(v);
    body->translate(_v);
}

void PhysObj::spawn(void *_node, Entity *en)
{
    //Entity *en = static_cast<Entity*>(node->getAttachedObject(0));
    SceneNode *node = static_cast<SceneNode*>(_node);
    BtOgre::StaticMeshToShapeConverter converter(en);

    if(shapeType == COLSHAPE_BOX)
        shape = converter.createBox();
    else if(shapeType == COLSHAPE_SPHERE)
        shape = converter.createSphere();
    else if(shapeType == COLSHAPE_CYLINDER)
        shape = converter.createCylinder();
    else if(shapeType == COLSHAPE_CONVEX)
        shape = converter.createConvex();
    else if(shapeType == COLSHAPE_TRIMESH)
        shape = converter.createTrimesh();
    else if(shapeType == COLSHAPE_EMPTY)
        shape = new btEmptyShape();
    shape->calculateLocalInertia(mass, inertia);
    physics::m_collisionShapes.push_back(shape);

    btVector3 _off = BtOgre::Convert::toBullet(Vector3(0, -.2, 0));

    BtOgre::RigidBodyState* mots;
    /* // center of mass, doesnt work yet
    if(0) { //!this->isWheel && this->mass > 0.0) {
        btTransform trans;
        //btTransform(BtOgre::Convert::toBullet(node->getOrientation()),
        //      BtOgre::Convert::toBullet(node->getPosition() + Vector3(0, .2, 0)));
        trans.setOrigin(-_off);
        btTransform trans2;
        trans2.setIdentity();
        trans2.setOrigin(_off);
        mots = new BtOgre::RigidBodyState(node, trans, trans2);
    }
    else {
    */
        mots = new BtOgre::RigidBodyState(node);

    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, mots, shape, inertia);
    body = new btRigidBody(rbInfo);
    /*
    if(this->isWheel)
        body->setActivationState(DISABLE_DEACTIVATION);
    */
    body->setFriction(friction);
    physics::m_dynamicsWorld->addRigidBody(body, colType, colWith);

    body->setUserPointer(this);
}

