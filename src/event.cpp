/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#include <iostream>
#include <zmq.hpp>
#include "event.h"

using namespace std;

Event* Event::m_pInstance = NULL;
Event* Event::Instance()
{
    if (!m_pInstance) // singleton
        m_pInstance = new Event;

    return m_pInstance;
}

Event::Event() :
    ctx(1),
    socket(ctx, ZMQ_PUSH)
{
}

bool Event::init()
{
    cout << "event init... ";

    socket.connect("tcp://127.0.0.1:5555");

    cout << "done" << endl;
    return true;
}

bool Event::send_event(string ev_name, string data)
{
    string msg_data = ev_name + " " + data;
    zmq::message_t msg(msg_data.size());
    memcpy(msg.data(), msg_data.c_str(), msg_data.size());

    // fixme
    Event *ev = Event::Instance();
    bool rc = ev->socket.send(msg, ZMQ_NOBLOCK);
    assert(rc == true);

    return true;
}
