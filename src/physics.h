/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef PHYSICS_H
#define PHYSICS_H
#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "obj.h"

namespace physics
{

extern btAlignedObjectArray<btCollisionShape*> m_collisionShapes;
extern btBroadphaseInterface*  m_broadphase;
extern btCollisionDispatcher*  m_dispatcher;
extern btConstraintSolver*     m_solver;
extern btDefaultCollisionConfiguration* m_collisionConfiguration;
extern btDynamicsWorld*        m_dynamicsWorld;
extern btCollisionWorld*       m_collisionWorld;
extern bool debug;
extern float mSimulationSpeed;

void init();
void deinit();
void update(float dt);
void setDebug(bool v);
void addFloor();

class Hinge
{
protected:
    btHingeConstraint* hinge;
    float mFriction;
public:
    Hinge(Obj *ob1, Obj *ob2, Quaternion basisA, Vector3 originA,
                              Quaternion basisB, Vector3 originB,
                              bool disableCollision=true);
    void rotateAngular(float vel, float impulse);
    void rotateToAngle(float angle, float dt);
    void setFriction(float val);
    float getMotorTargetVelosity();
    void enableMotor(bool val);
    void lock(bool lock=true, float softness=0.9, float bias=0.1);
    float getHingeAngle();
    void setAxis(Vector3 axis);
};

} // ::physics

#define BIT(x) (1<<(x))
enum collisiontypes {
    COL_NOTHING = 0, //<Collide with nothing
    COL_BIKE = BIT(0), //<Collide with bike
    COL_WALL = BIT(1), //<Collide with walls
    COL_POWERUP = BIT(2) //<Collide with powerups
};


#endif
