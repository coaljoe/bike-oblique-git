/* Oblique Bike
   Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details. */

#ifndef OBJECT_H
#define OBJECT_H
#include "Ogre.h"
#include "physobj.h"

using namespace Ogre;

class Obj
{
protected:
    SceneNode* mPivot;
    Entity* mEnt;
public:
    bool spawned;
    int id;
    std::string m_fn;
    Vector3 m_pos;
    Quaternion m_ori;
    PhysObj *phys;

    // Graphics
    std::string m_meshFile;
    std::string m_materialName;
    std::string m_name;

    void setMesh(std::string);
    void setMaterial(std::string);
    std::string getMaterialName() { return mEnt->getSubEntity(0)->getMaterialName(); }
    void setMaterialName(std::string s) { return mEnt->setMaterialName(s); }
    //char* getMesh() { return (char *)m_meshFile.c_str(); }
    SceneNode* getSceneNode() { return mPivot; }
    
    // Methods
    Obj();
    virtual ~Obj();
    bool load(std::string fn);

    Vector3 getPos() { return mPivot->getPosition(); }
    void setPos(Vector3 pos) {
        m_pos = pos;
        if(spawned) {
            mPivot->setPosition(m_pos);
            phys->setPos(m_pos);
        }
    }

    void setOri(Quaternion ori) {
        m_ori = ori;
        if(spawned) {
            mPivot->setOrientation(m_ori); // XXX fixme
            //phys->setOri(m_ori);
        }
    }

    void resetOriPos();
    void rotate(Vector3 axis, Radian angle, bool nophys=false);
    void spawn();
    void respawn();
    //void clone();
    void update(float dt) {}
};

/*
class StaticObj
{
};
*/

#endif
