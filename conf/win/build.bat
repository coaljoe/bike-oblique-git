call c:\vs2008\VC\vcvarsall.bat


call scons winlib


if %errorlevel% neq 0 goto error


cd sip
del cppview.lib
nmake clean
python configure.py
nmake


if %errorlevel% neq 0 goto error


mt -manifest cppview.pyd.manifest -outputresource:cppview.pyd;2


if %errorlevel% == 0 goto done else goto error


:error
@echo build error
@pause
goto exit

:done
copy /y cppview.pyd ..
@echo done
@pause

:exit