#!/bin/sh
##
## redist directory: bullet, ogreoggsound, OgreSDK, Python27 
##
dst=`pwd`/build/dist-test
pydir=c:/Python27


mkdir -p $dst/redist/pylib
pushd $pydir/Lib/site-packages
cp -r sip.pyd sipconfig.py sipdistutils.py \
      message-0.1.2-py2.7.egg \
      zmq \
      gi \
      gtk3 \
      $dst/redist/pylib
popd

pushd $dst/redist/pylib
unzip message-0.1.2-py2.7.egg message/*
popd

echo "** Now copy c:\Python27\[Lib,DLLs] manually to $dst/redist/pylib"

#rm -rf $dst/*
cp -r --parents redist/OgreSDK/media/materials/programs $dst
cp -r --parents redist/OgreSDK/media/RTShaderLib $dst
cp -r --parents redist/OgreSDK/bin/release $dst
cp redist/zeromq-2.2.0/lib/libzmq.dll $dst

rm -f redist/OgreSDK/bin/release/sample*
rm -f redist/OgreSDK/bin/release/Sample*

# copy project files
cp -r --parents data media py tests $dst

cp conf/win/*.cfg conf/win/run.bat $dst
cp cppview.pyd $dst

# cleanup project
find $dst -iname \*.py[co] -exec rm -f {} \;
rm -f $dst/*.log

# final steps
cp $WINDIR/system32/python27.dll $dst
cp $pydir/python.exe $dst

echo "done"
