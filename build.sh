#!/bin/sh
sudo cpupower frequency-set -u 2.7ghz
scons -j2 --max-drift=1 --implicit-deps-unchanged lib &&
cd sip
python2 configure.py || exit
make &&
mv -v cppview.so .. &&
cd ..
sudo cpupower frequency-set -u 1.5ghz
