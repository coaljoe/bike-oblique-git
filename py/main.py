#!/usr/bin/env python

# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
import sys
from time import sleep
sys.path.insert(0, '..')
import cppview
from src.conf import conf

conf.load()
conf.parse_args()
app = cppview.App()
if conf.args.nosound or conf.vars.nosound:
    app.setNoSound()
app.setup()
#app.addFloor()
#app.test()

print 'game import...'
from src.input import *
from src.game import Game
game = Game()
game._app = app

from src import gui
gui.init()
gui._game = game

init_input()
kl = cppview.KL()
kl.keypress = keypress_cb
kl.keyrelease = keyrelease_cb
kl.reg()

#game.stage.setLevel(args.level)
#game.replay.recorder.startRecord()
#game.replay.player.play('replay.json')
#game.stage.start()

if conf.args.level:
    game.playLevel(conf.args.level)
else:
    game.pause = True
    gui.show()

while 1:
    if not app.step():
        exit()
    if not game.pause:
        dt = app.getdt()
        game.update(dt)
    else:
        gui.update()
