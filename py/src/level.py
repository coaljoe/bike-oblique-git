# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
import os.path, fnmatch, math
from weakref import WeakSet
from cppview import Vector3, Quaternion, Radian
from src import conf
from src.obj import Obj, colshape
from src.dotscene import DotScene
from src.objects.strawberry import Strawberry
from src.export import export_blend


class Level(object):
    def __init__(self, levelname):
        self.name = levelname
        self.dirname = self.name
        self.size = 50

        self.bike_spawnpos = None
        self.strawberries = WeakSet()
        self.strawberries_total = 0
        self.dotscene = None
        self.objs = WeakSet()

    def spawn(self):
        if self.name.endswith('.blend'):
            # export .blend to levels/tmp
            export_blend(self.path)
            self.dirname = self.name + '_cache'
        # scene geometry
        filename = os.path.join(self.path, fnmatch.filter(os.listdir(self.path), '*.scene')[0])
        from src.game import Game
        app = Game()._app
        app.removeResGroup('level')
        app.addResLoc(self.path, 'level')
        app.resetScene()
        app.setDefaultScene()

        # load level.json
        leveljson = os.path.join(self.path, 'level.json')
        if os.path.exists(leveljson):
            app.loadLeveljson(leveljson)

        self.dotscene = DotScene(filename)
        #print dotscene.nodes
        for d in self.dotscene.nodes:
            ob = Obj()
            ob.setPos(Vector3(*d['position']))
            ob.setOri(Quaternion(*d['orientation']))
            #meshFile = 'levels/' + self.name + '/' + d['mesh']
            meshFile = d['mesh']
            print ">>>", meshFile
            ob.setMesh(meshFile)
            if d.get('physics_type') == 'NO_COLLISION':
                ob.phys.setShapeType(colshape.empty)
            else:
                prim = d.get('collisionPrim')
                if not prim:
                    ob.phys.setShapeType(colshape.trimesh)
                elif prim == 'box':
                    ob.phys.setShapeType(colshape.box)
            ob.spawn()

            # a post spawn material hack
            #print ob.getMaterialName()
            if ob.getMaterialName().startswith('Base_'):
                ob.setMaterialName("Examples/Water1")

            ob.phys.setStatic()
            self.objs.add(ob)
  
        #print dotscene.emptyNodes
        for d in self.dotscene.emptyNodes:
            if d['name'] == 'bike':
                self.bike_spawnpos = d['position']
            elif d['name'].startswith('flower'):
                s = Strawberry(isFlower=True)
                s.setPos(Vector3(*d['position']))
                s.spawn()
                self.objs.add(s)
  
        self.respawn_strawberries()
  
    def respawn_strawberries(self):
        for s in self.strawberries:
            s.destroy()
        self.strawberries.clear()
        self.strawberries_total = 0
        for d in filter(lambda k: k['name'].startswith('strawberry'), self.dotscene.emptyNodes):
            s = Strawberry()
            s.setPos(Vector3(*d['position']))
            s.spawn()
            self.objs.add(s)
            self.strawberries.add(s)
            self.strawberries_total += 1
  
    def free(self):
        for ob in self.objs: ob.destroy()
        self.objs.clear()
  
    def update(self, dt):
        v, inc = Vector3(0, 1, 0), Radian(math.radians(80*dt))
        for s in self.strawberries:
            s.rotate(v, inc, True)

    @property
    def path(self):
        return os.path.join('data', 'levels', self.dirname)


def getLevelsList():
    l = []
    for d in os.listdir(os.path.join(conf.data_path, 'levels')):
        if os.path.splitext(d)[1] not in ['', '.blend']: continue
        l.append(d)
    return l

