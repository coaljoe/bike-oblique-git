# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from time import time
from src.event import event
from src.util import Singleton
from src.objmgr import ObjMgr
from src.stage import Stage
from src.replay import Replay

class Timer(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.speed = 1.0

    def start(self):
        self.start_time = time()
        self.time = self.start_time

    def update(self, dt):
        self.time = (time() - self.start_time) * self.speed

timer = Timer()

class Game(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.frame = 0
        self.timer = timer
        self.timer.speed = 1.0
        self.timer.start()
        self.event = event
        self.objmgr = ObjMgr(self)
        self.replay = Replay(self)
        self.stage = Stage(self)
        self.shed_gen = None
        self._pause = False
        self._app = None
        self._lock = False

    def update(self, dt):
        dt = dt * self.timer.speed
        self.timer.update(dt)
        self.event.update(dt)
        self.replay.update(dt)

        #self.objmgr.update(dt)
        self.bike.update(dt)
        self.stage.update(dt)

        try: self.shed_gen.next()
        except: pass

        self.frame += 1

    def playLevel(self, val):
        self._lock = True
        if type(val) == int:
            from src.level import getLevelsList
            ll = getLevelsList()
            idx = (ll.index(self.stage.level.name) + val) % len(ll)
            val = ll[idx]
        self.stage.free()
        self.stage.setLevel(val)
        self.stage.start()
        self._lock = False

    @property
    def player(self):
        return self.playermgr.player

    @property
    def bike(self):
        return self.stage.bike

    @property
    def time(self):
        return self.timer.time

    @property
    def pause(self):
        return self._pause

    @pause.setter
    def pause(self, val):
        self._pause = val
        self._app.pause(val)

    def quit(self):
        import sip
        sip.delete(self._app)
