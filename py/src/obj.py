# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
import sip
from cppview import Obj as _Obj
from src import util
from src.game import Game

class Obj(_Obj):
    typename = "obj"

    def __init__(self):
        _Obj.__init__(self)
        self.name = util.get_autoname(self.typename)
        self.m_name = self.name
        self.spawned = False

    def __repr__(self):
        return "<Obj " + self.name + ">"

    #def __del__(self):
    #    #sip.delete(self)
    #    print "Obj del"
    #    #exit()

    #def __dtor__(self): # works when delete
    #    #sip.delete(self)
    #    print "Obj dtor"
    #    #exit()

    def spawn(self):
        print "SPAWN"
        Game().event.pub('ev_spawn', self)
        _Obj.spawn(self)
        self.spawned = True

    def respawn(self):
        print "RESPAWN"
        Game().event.pub('ev_respawn', self)
        _Obj.respawn(self)
        self.spawned = True

    def destroy(self):
        Game().event.pub('ev_destroy', self)
        self.delete()

    def delete(self):
        try:
            sip.delete(self)
        except RuntimeError:
            print 'err: seems the object already deleted'
        finally:
            print 'obj deleted', self

    def update(self, dt):
        pass


class colshape:
    box = 0
    sphere = 1
    cylinder = 2
    convex = 3
    trimesh = 4
    empty = 5
