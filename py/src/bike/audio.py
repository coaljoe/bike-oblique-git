# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from random import uniform
from cppview import Vector3
from src.geom import *
from src.util import DotDict
from src.audio import AudioObj
from src.conf import conf

class BikeAudioComplex(object):
    def __init__(self):
        self.sfx = DotDict()

    def update(self, dt):

        if self._forward:
            self.sfx.motor2.pause()
            cv = round(abs(self.rwheel.phys.getRelAngularVelocity().x), 3)
            p = cv/10
            self.sfx.motor.setVolume(p)
            if self.rwheel.rpm > 100:
                self.sfx.motor2.play()

        else:
            self.sfx.motor.mute()

        if self.rwheel.rpm < 100:
            self.sfx.motor2.pause()

        if self._brake:
            self.sfx.motor2.pause()


        if self.rwheel.isOnGround or self.fwheel.isOnGround:
        #if self.fwheel.isOnGround:
            if self.fwheel.rpm >= 70:
                v = lerp_abs(60, 190, self.fwheel.rpm)
                #print v
                self.sfx.tires.setVolume(v)
                self.sfx.tires.play()
            else:
                self.sfx.tires.pause()
        else:
            self.sfx.tires.pause()

    def respawn(self):
        self.sfx.motor = AudioObj('motor', 'sounds/motor1.ogg', True)
        self.sfx.motor2 = AudioObj('motor2', 'sounds/motor2.ogg', True)
        self.sfx.motor.attach(self.rwheel)
        self.sfx.motor2.attach(self.rwheel)
        self.sfx.tires = AudioObj('tires', 'sounds/tires.ogg', True)
        self.sfx.tires.attach(self.fwheel)
        self.sfx.tires.setVolume(0.8)

        #if not self._spawned:
        self.sfx.motor.mute()
        self.sfx.motor.setPitch(1.0)
        self.sfx.motor.play()
        self.sfx.motor2.setVolume(0.6)
        self.sfx.motor2.play()
        self.sfx.motor2.pause()


class BikeAudioSimple(object):
    def __init__(self):
        self.sfx = DotDict()

    def respawn(self):
        self.sfx.motor = AudioObj('motor', 'sounds/tone.ogg', True)
        self.sfx.motor.attach(self.rwheel)
        self.sfx.motor.setPitch(0.05)

    def update(self, dt):
        if self._forward:
            self.sfx.motor.play()
            cv = round(abs(self.rwheel.phys.getRelAngularVelocity().x), 3)
            p = cv/4
            #p += uniform(-1, 1)*1.0
            if p > 2.0:
                self.sfx.motor.setPitch(0.2 * p)
                self.sfx.motor.setVolume(0.8)
            else:
                self.sfx.motor.setVolume(0.4)
            #print p, self.rwheel.phys.getRelAngularVelocity().x
        else:
            self.sfx.motor.setPitch(0.05)
            self.sfx.motor.pause()


BikeAudio = BikeAudioComplex if not conf.vars.simple_audio else BikeAudioSimple

#class BikeAudio(BikeAudioSimple, BikeAudioComplex):
#    pass

