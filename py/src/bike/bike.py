# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from math import radians
from cppview import Vector3, Quaternion, Radian, Hinge
from src.geom import *
from src.conf import conf
from src.util import DotDict
from src.bike.wheel import Wheel, SupportWheel
from src.bike.frame import Frame
from src.bike.fork import Fork
from src.bike.audio import BikeAudio


class Bike(BikeAudio):
    "Default bike's proto"
    def __init__(self):
        self.fwheel = Wheel()
        self.rwheel = Wheel()
        self.frame = Frame()
        self.fork = Fork()
        self.lswheel = SupportWheel()
        self.rswheel = SupportWheel()
  
        self.motorTorque = 0.0
        self.motorTorqueMin = 2
        self.motorTorqueMax = 34
        
        self.forkRotateMin = 0.2
        self.forkRotateMax = 2.0
  
        self.opt = DotDict()
        self.opt.lockBar = conf.vars['lock_handlebar']
        self.opt.fullPull = conf.vars['full_pull']
  
        self._pullHandlebar = False
        self._forward = False
        self._brake = False
        self._steer = DotDict({'left': False, 'right': False, 'dir': None})
  
        self._spawned = False
        super(Bike, self).__init__()
  
    def spawn(self, pos=None):
        self.respawn(pos)
  
    def respawn(self, pos=None):
        #if self._spawned:
        #    self.delete()
        #    self._spawned = False
        for x in self.elements:
            x.resetOriPos()
        if pos:
            self.setPos(pos)
  
        a = Quaternion()
        a.FromAngleAxis(Radian(radians(90)), Vector3(0, 1, 0))
  
        self.fwheel.setOri(a)
        self.rwheel.setOri(a)
        self.lswheel.setOri(a)
        self.rswheel.setOri(a)
        self.fwheel.respawn()
        self.rwheel.respawn()
        self.fork.respawn()
        self.frame.respawn()
        self.lswheel.respawn()
        self.rswheel.respawn()
  
        # add frame-fork hinge
        originA = self.frame.fork_hinge_pos
        basisA = self.frame.fork_hinge_ori
        originB = self.fork.frame_hinge_pos
        basisB = self.fork.frame_hinge_ori
        #print self.frame, self.fork, basisA, originA, basisB, originB
        self.hingeFrameFork = Hinge(\
                self.frame, self.fork, basisA, originA, basisB, originB)
        self.hingeFrameFork.setFriction(0.02)
  
        # add fork-fwheel hinge
        origin = Vector3(0, 0, 0)
        basis = Quaternion()
        basisB = Quaternion()
        basisB.FromAngleAxis(Radian(radians(90)), Vector3(0, -1, 0))
        self.hingeForkFwheel = Hinge(\
                self.fork, self.fwheel, basisB, origin, basis, origin)
  
        # add frame-rwheel hinge
        originA = self.frame.rwheel_hinge_pos
        originB = Vector3(0, 0, 0)
        self.hingeFrameRwheel = Hinge(\
                self.frame, self.rwheel, basisB, originA, basis, originB)
  
        ## support wheels
  
        # add frame-lswheel hinge
        originA = self.frame.lswheel_hinge_pos
        originB = Vector3(0, 0, 0)
        basisA = Quaternion()
        basisA.FromAngleAxis(Radian(radians(90)), Vector3(0, -1, 0))
        basisB = Quaternion()
        basisB.FromAngleAxis(Radian(radians(5)), Vector3(1, 0, 0))
        self.hingeFrameLswheel = Hinge(\
                self.frame, self.lswheel, basisA, originA, basisB, originB)
  
        # add frame-rswheel hinge
        originA = self.frame.rswheel_hinge_pos
        originB = Vector3(0, 0, 0)
        basisB = Quaternion()
        basisB.FromAngleAxis(Radian(radians(5)), Vector3(-1, 0, 0))
        self.hingeFrameRswheel = Hinge(\
                self.frame, self.rswheel, basisA, originA, basisB, originB)
  
        # ololo
        self.lockBar()
        #self.hingeFrameRwheel.rotateToAngle(10) # motor
        #self.hingeForkFwheel.rotateToAngle(10) # motor
        #self.frame.phys.setLinearVelocity(Vector3(5.0, 0,0))
  
        super(Bike, self).respawn()
  
        [x.phys.disableDeactivation() for x in self.elements if hasattr(x, 'phys')]
  
        self._spawned = True
    
    def setPos(self, pos):
        _pos = Vector3(*pos)
        self.frame.setPos(_pos)
        self.fork.setPos(self.frame.fwheel_hinge_pos + _pos)
        self.fwheel.setPos(self.frame.fwheel_hinge_pos + _pos)
        self.rwheel.setPos(self.frame.rwheel_hinge_pos + _pos)
        self.lswheel.setPos(self.frame.lswheel_hinge_pos + _pos)
        self.rswheel.setPos(self.frame.rswheel_hinge_pos + _pos)
  
    
    def flip(self):
        _pos = self.frame.getPos()
        #self.hingeFrameFork.lock(False)
        """
        [x.phys.clearForces() for x in self.elements]
        #self.hingeFrameRwheel.setAxis(Vector3(0, 0, -1))
        #self.hingeForkFwheel.setAxis(Vector3(0, 0, -1))
        self.fwheel.rotate(Vector3(0, 1, 0), Radian(radians(45)))
        self.fork.rotate(Vector3(0, 1, 0), Radian(radians(45)))
        """
        [x.phys.clearForces() for x in self.elements]
        self.fork.setPos(_pos + (-self.frame.fwheel_hinge_pos))
        self.fwheel.setPos(_pos + (-self.frame.fwheel_hinge_pos))
        self.rwheel.setPos(_pos + (-self.frame.rwheel_hinge_pos))
        self.lswheel.setPos(_pos + (-self.frame.lswheel_hinge_pos))
        self.rswheel.setPos(_pos + (-self.frame.rswheel_hinge_pos))
        self.frame.rotate(Vector3(0, 1, 0), Radian(radians(180)))
        self.fork.rotate(Vector3(0, 1, 0), Radian(radians(180)))
        self.fwheel.rotate(Vector3(0, 1, 0), Radian(radians(180)))
        self.rwheel.rotate(Vector3(0, 1, 0), Radian(radians(180)))
        self.lswheel.rotate(Vector3(0, 1, 0), Radian(radians(180)))
        self.rswheel.rotate(Vector3(0, 1, 0), Radian(radians(180)))
        [x.phys.clearForces() for x in self.elements]
        #self.hingeFrameFork.setAxis(Vector3(0, 1, 0))
        #self.hingeFrameFork.lock()
        #self.fork.setPos(-self.frame.fwheel_hinge_pos + _pos)
  
    def delete(self):
        [x.delete() for x in self.elements]
  
    def lockBar(self, v=True):
        if not self.opt.lockBar: return
        self.hingeFrameFork.lock(v)
  
    # controller code
    def startForward(self):
        self._forward = True
        self.motorTorque = max(self.motorTorqueMin, self.motorTorque)
  
    def stopForward(self):
        self._forward = False
        self.motorTorque = min(self.motorTorque, self.motorTorqueMin)
        self.hingeFrameRwheel.enableMotor(False)
  
    def startBrake(self, handbrake=False):
        self._brake = True
        self.hingeFrameRwheel.setFriction(1)
        if handbrake:
            self.hingeFrameRwheel.lock(True, 0.5, 0)
  
    def stopBrake(self):
        self._brake = False
        self.hingeFrameRwheel.setFriction(0)
        self.hingeFrameRwheel.lock(False)
    
    def startSteer(self, dir):
        self._steer[dir] = True
        self._steer.dir = dir
        #print self._steer
        self.lockBar(False)
  
    def stopSteer(self, dir):
        self._steer[dir] = False
        a = [k for k,v in self._steer.items() if v==True]
        if a: self._steer.dir = a[0]
        #print self._steer
        if not self._steer.left and not self._steer.right:
            self.hingeFrameFork.enableMotor(False)
            self.lockBar()
  
    def startPullHandlebar(self):
        #print 'pullHandlebar'
        self._pullHandlebar = True
        if not self.opt.fullPull:
            self.fork.phys.applyForce(Vector3(0, 35, 0), Vector3(0, 0, 0))
  
    def stopPullHandlebar(self):
        self._pullHandlebar = False
        self.fork.phys.clearForces()
  
    def stabilize(self):
        #print 'bike::stabilize'
        #self.frame.phys.setLinearVelocity(Vector3(2.4, 0, 0))
        pass
        #print self.hingeFrameFork.getQuaternion()
  
    def update(self, dt):
        self.stabilize()
  
        if self._pullHandlebar:
            #print 'apply force'
            if self.opt.fullPull:
                self.fork.phys.applyForce(Vector3(0, 500 * dt, 0), Vector3(0, 0, 0))
  
        if self._forward:
            #print 'apply forward'
            inc = 50 * dt
            self.motorTorque = min(self.motorTorqueMax, self.motorTorque + inc)
            self.hingeFrameRwheel.rotateToAngle(self.motorTorque, dt)
  
        if self._brake:
            self.motorTorque = 0
  
        if self._steer.left or self._steer.right:
            smax = 6
            smin = 1.5
            a, b, t = self.forkRotateMin, self.forkRotateMax, max(min((self.speed-smin)/smax, 1), 0)
            ang = lerp(a, b, 1-t)
            #if self.speed < smin: ang = self.forkRotateMax
            #print ang, self.speed, t
            #ang = 2
            ang = -ang if self._steer.dir == 'right' else ang
            #print dir, ang, self.motorTorqueProgress
            self.hingeFrameFork.rotateAngular(ang, 0.1)


        super(Bike, self).update(dt)

        #print abs(self.rwheel.phys.getRelAngularVelocity().y)
        #print self.rwheel.rpm
  
    @property
    def elements(self):
        return [self.fwheel, self.rwheel, self.fork, self.frame, self.lswheel, self.rswheel]
  
    @property
    def motorTorqueProgress(self):
        """linear progress in motor torque 0 -> 1"""
        return lerp_abs(self.motorTorqueMin, self.motorTorqueMax, self.motorTorque)
  
    @property
    def speed(self):
        return self.frame.phys.getRelLinearVelocity().x
  
