# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from math import pi
from src.obj import Obj
from cppview import Vector3

class Wheel(Obj):
    "Default Wheel class"
    typename = "wheel"
    def __init__(self):
        Obj.__init__(self)
        self.radius = None
        print 'loading wheel...'
        self.load("data/objects/bike/wheel.json")
        print "wheel ok"
  
        # Obj's props
        #self.setMaterial("Examples/Chrome")
        #self.phys.setShapeType(2)
        #self.phys.setFriction(1000)
        self.phys.isWheel = True

    def respawn(self):
        Obj.respawn(self)
        self.phys.isWheel = True

    @property
    def isOnGround(self):
        v = self.getPos()
        v2 = Vector3(v)
        v2.y -= 0.4
        #print v, v2
        return self.phys.rayTest(v, v2)

    @property
    def rpm(self):
        m = 1/(2*pi) * 60
        return m * self.phys.getRelAngularVelocity().x

class SupportWheel(Obj):
    "Default Support class"
    typename = "supportwheel"
    def __init__(self):
        Obj.__init__(self)
        self.radius = None
        print 'loading support wheel...'
        self.load("data/objects/bike/supportwheel.json")
        print "wheel ok"
        self.phys.isWheel = True

    def respawn(self):
        Obj.respawn(self)
        self.phys.isWheel = True
