# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from math import radians
from src.obj import Obj
from cppview import Vector3, Quaternion, Radian

class Fork(Obj):
    "Bike's fork proto"
    typename = "fork"
    def __init__(self):
        Obj.__init__(self)
        self.frame_hinge_pos = Vector3(0, 0.5, 0)
        q1 = Quaternion()
        q1.FromAngleAxis(Radian(radians(90)), Vector3(1, 0, 0))
        q2 = Quaternion()
        q2.FromAngleAxis(Radian(radians(25)), Vector3(0, 1, 0))
        self.frame_hinge_ori = q1 * q2
        self.fwheel_hinge_pos = Vector3()
        self.fwheel_hinge_ori = Quaternion()
  
        print 'loading fork...'
        self.load("data/objects/bike/fork.json")
        print "wheel ok"
