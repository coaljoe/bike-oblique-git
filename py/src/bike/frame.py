# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from math import radians
from src.obj import Obj
from cppview import Vector3, Quaternion, Radian

class Frame(Obj):
    typename = "frame"
    def __init__(self):
        Obj.__init__(self)
        self.fwheel_hinge_pos = Vector3(0.50, 0, 0) # fixme
        self.rwheel_hinge_ori = Quaternion()
        self.rwheel_hinge_pos = Vector3(-0.480, 0, 0)
        self.rwheel_hinge_ori = Quaternion()
        self.fork_hinge_pos = Vector3(0.3, 0.45, 0)
        self.fork_hinge_ori = Quaternion()
        self.fork_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(1, 0, 0))
  
        # relative from frame's center
        self.lswheel_hinge_pos = Vector3(-0.48, -.2, -.32)
        self.lswheel_hinge_ori = Quaternion()
        self.rswheel_hinge_pos = Vector3(-0.48, -.2, .32)
        self.rswheel_hinge_ori = Quaternion()
  
        print 'loading frame...'
        self.load("data/objects/bike/frame.json")
        print "wheel ok"
  
