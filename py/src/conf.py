# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
import os, json, argparse
from src.util import Singleton, DotDict

data_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', '..', 'data')) # FIXME
media_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', '..', 'media'))
root_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', '..'))
src_path = os.path.join(os.path.dirname(__file__))

class Conf(object):
    __metaclass__ = Singleton
    def __init__(self):
        self.data_path = data_path
        self.media_path = media_path
        self.src_path = src_path
        # default vars
        self.vars = DotDict({'blender_path': 'blender', 'nosound': False, 'lock_handlebar': True, 'full_pull': False, 'simple_audio': False})
        # run args
        self.args = None
        self.conffile = os.path.join(root_path, 'config.json')
        print 'using conf:', self.conffile

    def parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--level", type=str, help="level to play", metavar="NAME")
        parser.add_argument("--nosound", action='store_true', help="disable sound")
        self.args = parser.parse_args()

    def load(self):
        if os.path.exists(self.conffile):
            self.vars.update(json.load(open(self.conffile, 'r')))
        else:
            self.save()

    def save(self):
        json.dump(self.vars, open(self.conffile, 'w+'), indent=2)

conf = Conf()
