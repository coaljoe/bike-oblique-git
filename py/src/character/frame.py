from __future__ import division, absolute_import
#from weakref import WeakValueDictionary
from math import radians
from cppview import Obj
from cppview import Vector3, Quaternion, Radian

class Frame(Obj):
  def __init__(self):
      Obj.__init__(self)
      self.rwheel_hinge_pos = Vector3()
      self.rwheel_hinge_ori = Quaternion()
      self.fork_hinge_pos = Vector3()
      self.fork_hinge_ori = Quaternion()

      # Obj's props
      self.setMaterial("Examples/RustySteel")
      self.phys.setShapeType(3)

#framesCol = WeakValueDictionary()

def get(name):
    if name == 'default':
        ob = Frame()
        ob.rwheel_hinge_pos = Vector3(-0.480, 0, 0)
        #ob.rwheel_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(0, 0, 1))
        ob.fork_hinge_pos = Vector3(0.5, 0.5, 0)
        ob.fork_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(0, 0, -1))
        ob.setMesh('objects/frame.mesh')
        ob.phys.setMass(10.0 + 50.0)
        return ob

# default frame
"""
ob = Frame()
ob.rwheel_hinge_pos = Vector3(-0.480, 0, 0)
ob.rwheel_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(0, 0, 1))
ob.fork_hinge_pos = Vector3(0.5, 0.5, 0)
ob.fork_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(1, 0, 0))
ob.phys.setMass(10.0)
framesCol['default'] = ob
"""
