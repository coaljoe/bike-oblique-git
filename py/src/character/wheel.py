from __future__ import division, absolute_import
import cppview
from weakref import WeakValueDictionary

wheelsCol = WeakValueDictionary()

class Wheel(cppview.Obj):
  "Default Wheel class"
  def __init__(self):
      cppview.Obj.__init__(self)
      self.radius = None

      # Obj's props
      self.setMaterial("Examples/Chrome")
      self.phys.setShapeType(2)
      self.phys.setFriction(1000)

  def clone(self):
      ret = cppview.Obj(self)
      ret.__dict__.update(self.__dict__)
      return ret


"""
  def spawn(self):
      Obj.spawn(self)
"""

def init():
    print '>>>>>>>>>>>>>> wheel init'
    # default
    global wheelsCol
    ob = Wheel()
    ob.radius = 26
    ob.setMesh("objects/wheel.mesh")
    ob.phys.setMass(2.5)
    wheelsCol['default'] = ob

def get(name):
    if name == 'default':
        ob = Wheel()
        ob.radius = 26
        ob.setMesh("objects/wheel.mesh")
        ob.phys.setMass(4.5)
        return ob


"""
# wheel20in
ob = Wheel()
ob.radius = 20
ob.setMesh("objects/wheel20.mesh")
ob.phys.setMass(2.5)
wheelsCol['wheel20in'] = ob
"""

