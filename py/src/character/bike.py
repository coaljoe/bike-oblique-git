from __future__ import division, absolute_import
from math import radians
from cppview import Vector3, Quaternion, Radian, Obj
#from src import physics 
from src.bike import wheel, fork, frame


class Character(object):
  "Default bike's proto"
  def __init__(self):
      #self.fwheel = wheel.wheelsCol['default'].clone()
      #self.rwheel = wheel.wheelsCol['default'].clone()
      self.fwheel = wheel.get('default')
      self.rwheel = wheel.get('default')
      self.frame = frame.get('default')
      self.fork = fork.get('default')
      self.model = Obj() # body
      self.model.setMesh('objects/robot.mesh')

  def spawn(self):
      self.model.spawn()

  # controller code
  def moveForward(self):
      self.frame.phys.setLinearVelocity(Vector3(5.0, 0,0))
      #self.hingeFrameRwheel.rotateToAngle(10)
      #self.hingeForkFwheel.rotateToAngle(10)
      pass

  def moveBack(self):
      self.frame.phys.setLinearVelocity(Vector3(-0.5, 0, 0))
  
  def moveLeft(self):
      print 'takeLeft'
      #self.fork.phys.hingeRotateToAngle('FrameFork', -0.58)
      #self.fork.phys.getHinge('FrameFork').rotateToAngle(-0.58)
      self.hingeFrameFork.rotateAngular(0.58, 5)
      #physics.physics.rotateToAngle(self.fork, -0.58)

  def moveRight(self):
      print 'takeRight'
      print self.fork
      self.hingeFrameFork.rotateAngular(-0.58, 5)
      #self.fork.phys.rotateToAngle(0.58)
      #physics.physics.rotateToAngle(self.fork, 0.58)


