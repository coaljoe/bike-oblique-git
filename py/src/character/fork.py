from __future__ import division, absolute_import
#from weakref import WeakValueDictionary
from math import radians
from cppview import Obj, Vector3, Quaternion, Radian


class Fork(Obj):
  "Bike's fork proto"
  def __init__(self):
      Obj.__init__(self)
      self.frame_hinge_pos = Vector3()
      self.frame_hinge_ori = Quaternion()
      self.fwheel_hinge_pos = Vector3()
      self.fwheel_hinge_ori = Quaternion()

      # Obj's props
      self.setMaterial("Examples/RustySteel")
      self.phys.setShapeType(3)

#forksCol = WeakValueDictionary()
def get(name):
    if name == 'default':
        # default fork
        ob = Fork()
        ob.frame_hinge_pos = Vector3(0, 0.5, 0)
        #ob.fwheel_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(0, 0, -1))
        ob.setMesh('objects/fork.mesh')
        ob.phys.setMass(3 + 15.0)
        return ob

"""
# default fork
ob = Fork()
ob.frame_hinge_pos = Vector3(0, 0.5, 0)
#ob.fwheel_hinge_ori.FromAngleAxis(Radian(radians(90)), Vector3(1, 0, 0))
ob.phys.setMass(2.5)
forksCol['default'] = ob
"""


