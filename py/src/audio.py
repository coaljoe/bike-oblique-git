# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from src.util import Singleton
from src.conf import conf
from cppview import AudioObj as _AudioObj

disable = conf.args.nosound or conf.vars.nosound

class AudioObj(object):
    """a fakable audio object"""
    def __init__(self, *args, **kw):
        if not disable:
            self.obj = _AudioObj(*args)
    def __getattr__(self, attr):
        if not disable:
            return getattr(self.obj, attr)
        else:
            return lambda *args, **kw: None

    def mute(self):
        self.setVolume(0)


class Sfx(object):
    __metaclass__ = Singleton

    def __init__(self):
        self.sfx = {}
        #print '...', disable
        self.sfx['pick'] = AudioObj('pick', 'sounds/pick1.ogg')
        self.sfx['pick'].setVolume(0.5)
        #self.sfx['bg'] = AudioObj('bg', 'sounds/bg.wav', True)
        #self.sfx['bg'].setVolume(0)

sfx = Sfx().sfx
