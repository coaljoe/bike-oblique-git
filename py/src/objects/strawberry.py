# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from src.obj import Obj

class Strawberry(Obj):
    typename = 'strawberry'
    def __init__(self, isFlower=False):
        Obj.__init__(self)
        self.isFlower = isFlower
        print 'loading strawberry...'
        if not self.isFlower:
            self.load("data/objects/strawberry.json")
        else:
            self.load("data/objects/flower.json")
        print "strawberry ok"

    def onCollide(self, oth):
        if self.isFlower:
            return

        print self.name, "onCollide...", oth
        self.destroy()

    def spawn(self):
        Obj.spawn(self)
        self.phys.setNoResponse()

    #def __del__(self):
    #    print 'Strawberry del'

    #def __dtor__(self):
    #    print 'Strawberry dtor'
