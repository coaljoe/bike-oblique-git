# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from src.event import event

# http://gamekit.googlecode.com/svn/trunk/Dependencies/Source/OIS/include/OISKeyboard.h
KC_UNASSIGNED  = 0x00
KC_ESCAPE      = 0x01
KC_1           = 0x02
KC_2           = 0x03
KC_3           = 0x04
KC_4           = 0x05
KC_5           = 0x06
KC_6           = 0x07
KC_7           = 0x08
KC_8           = 0x09
KC_9           = 0x0A
KC_0           = 0x0B
KC_MINUS       = 0x0C
KC_EQUALS      = 0x0D
KC_BACK        = 0x0E
KC_TAB         = 0x0F
KC_Q           = 0x10
KC_W           = 0x11
KC_E           = 0x12
KC_R           = 0x13
KC_T           = 0x14
KC_Y           = 0x15
KC_U           = 0x16
KC_I           = 0x17
KC_O           = 0x18
KC_P           = 0x19
KC_LBRACKET    = 0x1A
KC_RBRACKET    = 0x1B
KC_RETURN      = 0x1C
KC_LCONTROL    = 0x1D
KC_A           = 0x1E
KC_S           = 0x1F
KC_D           = 0x20
KC_F           = 0x21
KC_G           = 0x22
KC_H           = 0x23
KC_J           = 0x24
KC_K           = 0x25
KC_L           = 0x26
KC_SEMICOLON   = 0x27
KC_APOSTROPHE  = 0x28
KC_GRAVE       = 0x29
KC_LSHIFT      = 0x2A
KC_BACKSLASH   = 0x2B
KC_Z           = 0x2C
KC_X           = 0x2D
KC_C           = 0x2E
KC_V           = 0x2F
KC_B           = 0x30
KC_N           = 0x31
KC_M           = 0x32
KC_COMMA       = 0x33
KC_PERIOD      = 0x34
KC_SLASH       = 0x35
KC_RSHIFT      = 0x36
KC_MULTIPLY    = 0x37
KC_LMENU       = 0x38
KC_SPACE       = 0x39
KC_CAPITAL     = 0x3A
KC_F1          = 0x3B
KC_F2          = 0x3C
KC_F3          = 0x3D
KC_F4          = 0x3E
KC_F5          = 0x3F
KC_F6          = 0x40
KC_F7          = 0x41
KC_F8          = 0x42
KC_F9          = 0x43
KC_F10         = 0x44
KC_NUMLOCK     = 0x45
KC_SCROLL      = 0x46
KC_NUMPAD7     = 0x47
KC_NUMPAD8     = 0x48
KC_NUMPAD9     = 0x49
KC_SUBTRACT    = 0x4A
KC_NUMPAD4     = 0x4B
KC_NUMPAD5     = 0x4C
KC_NUMPAD6     = 0x4D
KC_ADD         = 0x4E
KC_NUMPAD1     = 0x4F
KC_NUMPAD2     = 0x50
KC_NUMPAD3     = 0x51
KC_NUMPAD0     = 0x52
KC_DECIMAL     = 0x53
KC_OEM_102     = 0x56
KC_F11         = 0x57
KC_F12         = 0x58
KC_F13         = 0x64
KC_F14         = 0x65
KC_F15         = 0x66
KC_KANA        = 0x70
KC_ABNT_C1     = 0x73
KC_CONVERT     = 0x79
KC_NOCONVERT   = 0x7B
KC_YEN         = 0x7D
KC_ABNT_C2     = 0x7E 
KC_NUMPADEQUALS= 0x8D 
KC_PREVTRACK   = 0x90 
KC_AT          = 0x91 
KC_COLON       = 0x92 
KC_UNDERLINE   = 0x93 
KC_KANJI       = 0x94
KC_STOP        = 0x95
KC_AX          = 0x96
KC_UNLABELED   = 0x97
KC_NEXTTRACK   = 0x99
KC_NUMPADENTER = 0x9C
KC_RCONTROL    = 0x9D
KC_MUTE        = 0xA0
KC_CALCULATOR  = 0xA1
KC_PLAYPAUSE   = 0xA2
KC_MEDIASTOP   = 0xA4
KC_VOLUMEDOWN  = 0xAE
KC_VOLUMEUP    = 0xB0 
KC_WEBHOME     = 0xB2 
KC_NUMPADCOMMA = 0xB3    
KC_DIVIDE      = 0xB5 
KC_SYSRQ       = 0xB7
KC_RMENU       = 0xB8    
KC_PAUSE       = 0xC5    
KC_HOME        = 0xC7    
KC_UP          = 0xC8    
KC_PGUP        = 0xC9    
KC_LEFT        = 0xCB    
KC_RIGHT       = 0xCD    
KC_END         = 0xCF    
KC_DOWN        = 0xD0    
KC_PGDOWN      = 0xD1    
KC_INSERT      = 0xD2    
KC_DELETE      = 0xD3    
KC_LWIN        = 0xDB    
KC_RWIN        = 0xDC    
KC_APPS        = 0xDD    
KC_POWER       = 0xDE    
KC_SLEEP       = 0xDF    
KC_WAKE        = 0xE3    
KC_WEBSEARCH   = 0xE5    
KC_WEBFAVORITES= 0xE6    
KC_WEBREFRESH  = 0xE7    
KC_WEBSTOP     = 0xE8    
KC_WEBFORWARD  = 0xE9    
KC_WEBBACK     = 0xEA    
KC_MYCOMPUTER  = 0xEB    
KC_MAIL        = 0xEC    
KC_MEDIASELECT = 0xED     

game = None

def init_input():
    from src.game import Game
    global game
    game = Game()


def keypress_cb(kc):
    if game._lock: return

    #print 'press'
    if kc == KC_P:
        game.pause = not game.pause
    if game.pause:
        return
    event.pub('ev_keypress', kc)

    b = game.bike
    if kc == KC_Q:
        print 'exit'
        game.quit()
        exit()
    if kc == KC_UP:
        b.startForward()
    if kc == KC_DOWN:
        b.startBrake()
    if kc == KC_LEFT:
        b.startSteer(dir='left')
    if kc == KC_RIGHT:
        b.startSteer(dir='right')
    if kc == KC_C:
        b.startPullHandlebar()
    if kc == KC_SPACE:
        b.flip()
    if kc in (KC_W, KC_Z):
        b.startBrake(handbrake=True)
    if kc == KC_RETURN:
        game.stage.restart()
    if kc == KC_TAB:
        game._app.getPlayerCam().setCamType(-1)
    if kc == KC_PGUP:
        game._lock = True
        game.playLevel(+1)
    if kc == KC_PGDOWN:
        game._lock = True
        game.playLevel(-1)


def keyrelease_cb(kc):
    if game._lock: return

    #print 'release'
    if game.pause:
        return
    event.pub('ev_keyrelease', kc)

    b = game.bike
    if kc == KC_LEFT:
        b.stopSteer(dir='left')
    if kc == KC_RIGHT:
        b.stopSteer(dir='right')
    if kc == KC_UP:
        b.stopForward()
    if kc in (KC_DOWN, KC_W, KC_Z):
        b.stopBrake()
    if kc == KC_C:
        b.stopPullHandlebar()
    if kc == KC_F7:
        game._app.togglePhysDebug()
