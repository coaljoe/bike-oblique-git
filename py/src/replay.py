# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
import os.path, json
from src.event import event
from src.conf import root_path
from src.input import *

class Replay(object):
    def __init__(self, game):
        self.recorder = Recorder(game)
        self.player = Player(game)

    def update(self, dt):
        if self.player.playing:
            self.player.update(dt)

# ? singleton
class Recorder(object):
    def __init__(self, game):
        self.game = game
        self.keys = []

    def startRecord(self):
        event.sub('ev_keypress', self.onKeyPress)
        event.sub('ev_keyrelease', self.onKeyRelease)
        event.sub('ev_stage_done', self.onStageDone)

    def stopRecord(self):
        pass

    def onKeyPress(self, kc):
        self.keys.append([self.game.stage.time, 1, kc])

    def onKeyRelease(self, kc):
        self.keys.append([self.game.stage.time, 0, kc])

    def onStageDone(self):
        json.dump(self.keys, open(os.path.join(root_path, 'replay.json') , 'w+'), indent=2)


class Player(object):
    def __init__(self, game):
        self.game = game
        self.keys = []
        self.t_last = None
        self.tcorr = 0.0
        self.tdesync = 0.0
        self.playing = False

    def play(self, file):
        self.keys = json.load(open(os.path.join(root_path, file), 'r'))
        self.playing = True

    def update(self, dt):
        tc = self.game.stage.time
        i = 0
        for t, press, kc in self.keys:
            i += 1
            if t <= self.t_last: continue
            #if round(t, 2) == round(tc, 2) and t != self.t_last:
            if tc+self.tcorr >= t and t != self.t_last:
                self.tcorr += (tc-t)
                print t, press, kc, tc-t, i, self.tcorr, tc-(t+self.tcorr)
                if press:
                    keypress_cb(kc)
                else:
                    keyrelease_cb(kc)
                self.t_last = t
                continue
