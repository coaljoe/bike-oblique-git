# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
import os, subprocess, shutil, hashlib
from ..conf import src_path, data_path, conf
BlenderPath = conf.vars['blender_path']

def export_blend(path):
    tmppath = path + '_cache'
    srcmd5 = hashlib.md5(open(path, 'r').read()).hexdigest() 
    digest_file = os.path.join(tmppath, 'digest.md5')

    # check sums
    if os.path.exists(digest_file):
        sum = open(digest_file).read()
        if sum == srcmd5:
            return

    # clear tmpdir
    try: os.mkdir(tmppath)
    except OSError: shutil.rmtree(tmppath); os.mkdir(tmppath)

    out = os.path.join(tmppath, 'tmp.scene')
    # clear python env
    _env = os.environ.copy()
    del _env['PYTHONPATH']
    _env['BLENDER_USER_SCRIPTS'] = os.path.join(src_path, 'export')
    subprocess.check_call([BlenderPath, '-b', '-b', path, '-P',
                            os.path.join(src_path, 'export', 'script_blender2ogre.py'), '--', out], env=_env)

    if os.path.exists(out):
        # write digest
        open(digest_file, 'w+').write(srcmd5)
    else:
        print 'export error'
        return -1

