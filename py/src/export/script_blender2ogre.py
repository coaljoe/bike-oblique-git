# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import absolute_import
import bpy, os, sys

# get output .scene file path
output = sys.argv[7]
print(output)

# activate all layers
bpy.context.scene.layers = [True] * 20

# apply modifiers and scale/rotation
for ob in [x for x in bpy.data.objects if x.type in ('MESH', 'EMPTY')]:
    print('->', ob.name)
    bpy.ops.object.select_all(action='DESELECT')
    ob.select = True
    bpy.context.scene.objects.active = ob
    #import pdb; pdb.set_trace()
    if ob.type == 'MESH':
        bpy.ops.object.convert(target='MESH', keep_original=False)
    bpy.ops.object.transform_apply(rotation=True, scale=True)
    if ob.type == 'MESH':
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.normals_make_consistent()
        bpy.ops.object.editmode_toggle()


# save .json texts
d = bpy.data.texts
for k in [k for k in d.keys() if k.endswith('.json')]:
    print("saving text:", k)
    f = open(os.path.join(os.path.dirname(output), k), 'w+')
    f.writelines(["%s\n" % line.body for line in d[k].lines])
    f.close()
    print("done")

# internal export script
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
print("exporting level...")
import io_export_ogreDotScene as es
if sys.platform == 'win32':
    _path = os.path.abspath(r'redist\OgreSDK\bin\release\OgreXMLConverter.exe')
    print('->', _path)
    if not os.path.isfile(_path):
        print('CANNOT FIND OgreXMLConverter. exit.')
        exit(-1)
    es._CONFIG_DEFAULTS_WINDOWS['OGRETOOLS_XML_CONVERTER'] = _path
    es.CONFIG['OGRETOOLS_XML_CONVERTER'] = _path

es.register()
bpy.ops.ogre.export(filepath=output, EX_generateEdgeLists=True, EX_SELONLY=False, EX_SCENE=True, EX_MESH=True)
#import pdb; pdb.Pdb(skip=['xml.*']).set_trace()
es.unregister()
print("done")

#sys.exit(0)
