# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import os
from gi.repository import Gtk, Gdk
from src.event import event
from src.conf import conf
from input import *

win = None
_game = None

class MainMenu(object):
    def __init__(self):
        global win
        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(conf.data_path, 'gui', 'mainmenu.glade'))
        self.builder.connect_signals(self) 
        win = self.builder.get_object("main")
        self.ll = self.builder.get_object('levelslist')
        from src.level import getLevelsList
        for d in getLevelsList():
            self.ll.append([d, '-'])
        self.nb = self.builder.get_object('notebook')
        self.builder.get_object('blender_executable').set_filename(conf.vars['blender_path'])
        self.builder.get_object('lock_handlebar').set_active(conf.vars['lock_handlebar'])
        self.builder.get_object('full_pull').set_active(conf.vars['full_pull'])
        self.builder.get_object('simple_audio').set_active(conf.vars['simple_audio'])
        win.set_modal(True)
        win.set_keep_above(True)
        event.sub('ev_keyrelease', self.onKeyRelease)
        win.connect("delete-event", Gtk.main_quit)
        win.connect("key-release-event", self.on_keyrelease)

    def onKeyRelease(self, kc):
        if _game.pause: return
        if kc == KC_ESCAPE:
            _game.pause = True
            win.show()

    def on_keyrelease(self, _, event):
        if event.keyval == Gdk.KEY_Escape:
            if _game.pause:
                leavemenu()

    def on_play_clicked(self, widget):
        lb = self.builder.get_object('levelsbox')
        selection = lb.get_selection()
        model, paths = selection.get_selected_rows()
        it = model.get_iter(paths[0])
        val = model.get(it, 0)[0]
        _game.playLevel(val)
        leavemenu()

    def on_levels_clicked(self, w):
        self.nb.set_current_page(0)

    def on_options_clicked(self, w):
        self.nb.set_current_page(1)

    def on_levelsbox_key_press_event(self, w, e):
        if e.keyval == Gdk.KEY_Return:
            self.on_play_clicked(w)

    def on_blender_executable_file_set(self, w):
        conf.vars['blender_path'] = w.get_filename()

    def on_lock_handlebar_toggled(self, w):
        v = w.get_active()
        conf.vars['lock_handlebar'] = v
        if _game:
            _game.bike.opt.lockBar = v

    def on_full_pull_toggled(self, w):
        v = w.get_active()
        conf.vars['full_pull'] = v
        if _game:
            _game.bike.opt.fullPull = v

    def on_simple_audio_toggled(self, w):
        conf.vars['simple_audio'] = w.get_active()
        print 'restart needed'

    def on_main_visibility_notify_event(self, w, _):
        self.builder.get_object('levelsbox').grab_focus()

    def on_save_options_clicked(self, w):
        conf.save()

    def on_exit_clicked(self, widget):
        _game.quit()
        exit()


def leavemenu():
    """leave mainmenu"""
    _game.pause = False
    win.hide()

def toggle():
    global win
    win.hide() if win.get_visible() else win.show()

def show(): win.show()
def hide(): win.hide()


def init():
    global win
    #win.fullscreen()
    #win.show_all()

    #Gdk.threads_init()
    screen = Gdk.Screen.get_default()

    css_provider = Gtk.CssProvider()
    css_provider.load_from_path(os.path.join(conf.data_path, 'gui', 'gtk.css'))

    context = Gtk.StyleContext()
    context.add_provider_for_screen(screen,
                                    css_provider,
                                    Gtk.STYLE_PROVIDER_PRIORITY_USER)

    mm = MainMenu()
    ss = Gtk.Settings()
    ss.set_long_property("gtk-timeout-initial", 0, 'main')
    ss.set_long_property("gtk-timeout-repeat", 0, 'main')

def main():
    Gtk.main()

def update():
    while Gtk.events_pending():
        Gtk.main_iteration()
