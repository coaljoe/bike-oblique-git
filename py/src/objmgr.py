# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from weakref import WeakValueDictionary
from src.event import event

class ObjMgr(object):
    def __init__(self, game):
        self.game = game
        self.objs = []
        self.objs_map = WeakValueDictionary()

        event.sub('ev_collide', self.onCollide)
        event.sub('ev_spawn', self.onSpawn)
        event.sub('ev_respawn', self.onRespawn)
        event.sub('ev_destroy', self.onDestroy)

    def id2obj(self, id):
        pass

    def onSpawn(self, obj):
        self.objs.append(obj)
        self.objs_map[obj.name] = obj

    def onRespawn(self, obj):
        # register object if not spawned
        if not obj.spawned:
            self.onSpawn(obj)

    def onDestroy(self, obj):
        #print 'on_destroy', obj
        self.objs.remove(obj)
        del self.objs_map[obj.name]
        
    def onCollide(self, obj1_name, obj2_name):
        obj1 = self.objs_map.get(obj1_name)
        obj2 = self.objs_map.get(obj2_name)
        if not (obj1 or obj2):
            print 'false collision with non-existent object, skipped'
            return
        event.pub('ev_objscollide', obj1, obj2)
        
        for obj in (obj1, obj2):
            opposite_obj = obj1 if obj == obj2 else obj2
            if hasattr(obj, 'onCollide'):
                obj.onCollide(opposite_obj)

    def update(self, dt):
        for x in self.objs:
            x.update(dt)

