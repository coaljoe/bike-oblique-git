# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from lib import message
from src.util import Singleton
import zmq

class Event(object):
    __metaclass__ = Singleton
    """Game's event subsystem
    """
    def __init__(self):
        self.ctx = zmq.Context(1)
        self.socket = self.ctx.socket(zmq.PULL)
        self.socket.bind('tcp://127.0.0.1:5555')

    def sub(self, evname, handler):
        message.sub(evname, handler)

    def pub(self, evname, *args, **kw):
        message.pub(evname, *args, **kw)

    def update(self, dt):
        data = None
        try:
            data = self.socket.recv(flags=zmq.NOBLOCK)
        except zmq.ZMQError:
            return

        ev_name = data.split(" ")[0]
        args = data.split(" ")[1:]
        self.pub(ev_name, *args)

event = Event()

