# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from __future__ import division, absolute_import
from time import time
from src.audio import sfx
from src.event import event

class Stage(object):
    def __init__(self, game):
        self.game = game
        self.level = None
        self.spawn_pos = None
        from src.bike import bike
        self.bike = bike.Bike()
        event.sub('ev_objscollide', self.onObjsCollide)
        self.start_time = None
        self._reset()

    def _reset(self):
        self.strawberries_collected = 0

    def restart(self):
        self._reset()
        self.level.respawn_strawberries()
        self.bike.respawn(self.spawn_pos)

    def setLevel(self, name):
        print 'setLevel', name
        from src.level import Level
        self.levelName = name
        self.level = Level(self.levelName)
        self.level.spawn()

    def start(self):
        self.start_time = time()
        # fixme: metadata must be loaded before level spawned
        self.spawn_pos = self.level.bike_spawnpos
        self.strawberries_total = self.level.strawberries_total
        print self.spawn_pos
        if not self.bike._spawned:
            self.bike.spawn(self.spawn_pos)
            # fixme
            if self.bike.fork:
                cam = self.game._app.getPlayerCam()
                cam.setAutoTracking(self.bike.fork)
        else:
            self.bike.respawn(self.spawn_pos)
        print "STAGE START!"
        #sfx['bg'].play()

    def done(self):
        print "STAGE DONE!"
        event.pub('ev_stage_done')
        self.restart()

    def free(self):
        if self.level:
            self.level.free()
        self._reset()
        #sfx['bg'].pause()

    def update(self, dt):
        self.level.update(dt)

    def onObjsCollide(self, ob1, ob2):
        for ob in (ob1, ob2):
            if not ob or ob.typename != 'strawberry':
                continue

            if ob.isFlower:
                if self.strawberries_collected == self.strawberries_total:
                    self.done()
            else:
                print "COLLECTED STRAWBERRY"
                self.strawberries_collected += 1
                sfx['pick'].play()

    @property
    def time(self):
        return time() - self.start_time
