# Oblique Bike
# Copyright (C) 2013 Aleksandr Kovtun <moorland@lavabit.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

"""
this file parses .scene node (dotscene) files and
creates them in OGRE with user data

Doesn't do any fancy stuff (skydome, XODE, lightmapping, etc) but you can use this as a base for adding those features.)

cpp:
http://www.ogre3d.org/wiki/index.php/DotScene_Loader_with_User_Data_Class
"""
from xml.dom import minidom, Node
import os.path


class DotScene(object):
    def __init__ (self, fileName, rootNode=None):
        self.fileName = fileName
        self.cameras = []
        self.lights = []
        self.nodes = []
        self.emptyNodes = []
        self.prefix = '' #prefix # used to prefix the node name when creating nodes
        self.entity_postfix = '' #entity_postfix 
        nodes = self.findNodes(minidom.parse(self.fileName).documentElement,'nodes')
        self.root = nodes[0].childNodes

        self.rootNode = rootNode
                    
        self.parseDotScene()


    
    # allows self['nodeName'] to reference xml node in '<nodes>'
    def __getitem__ (self,name):
        return self.findNodes(self.root,name)

    def parseDotScene (self):
        # TODO: check DTD to make sure you get all nodes/attributes
        # TODO: Use the userData for sound/physics
        for node in self.root:
            if node.nodeType == Node.ELEMENT_NODE and node.nodeName == 'node':
                realName =  node.attributes['name'].nodeValue
                if realName.startswith('_array_'):
                    print 'array nodes not supported', realName
                    continue
                # create new scene node
                newNode = {} # self.prefix + realName)

                #position it
                pos = self.findNodes(node, 'position')[0].attributes
                newNode['position'] = (float(pos['x'].nodeValue), float(pos['y'].nodeValue), float(pos['z'].nodeValue))
                
                # rotate it
                rot = self.findNodes(node, 'rotation')[0].attributes
                newNode['orientation'] = (float(rot['qw'].nodeValue), float(rot['qx'].nodeValue),
                                                      float(rot['qy'].nodeValue), float(rot['qz'].nodeValue))
                #print 'ROT', float(rot['w'].nodeValue), float(rot['x'].nodeValue),
                #             float(rot['y'].nodeValue), float(rot['z'].nodeValue)
                
                # scale it
                scale = self.findNodes(node, 'scale')[0].attributes
                newNode['scale'] = (float(scale['x'].nodeValue), float(scale['y'].nodeValue), float(scale['z'].nodeValue))
                
                
                # is it an entity?
                #print node, realName
                try:
                    thingy = self.findNodes(node, 'entity')[0].attributes
                except IndexError:
                    newNode['name'] = str(realName)
                    self.emptyNodes.append(newNode)
                    print 'added empty: "%s"' % (newNode['name'])
                    continue
                name = str(thingy['name'].nodeValue + self.entity_postfix)
                mesh = str(thingy['meshFile'].nodeValue)
                #mesh = str(os.path.join(self.dir_path, thingy['meshFile'].nodeValue))
                #attachMe = self.sceneManager.createEntity(name,mesh)
                print 'added entity: "%s" %s' % (name, mesh)

                # attach it to the scene
                #try:
                #newNode.attachObject(attachMe)
                #print 'attached', attachMe
                #except:
                #    print "could not attach:",realName
                
                newNode['name'] = name
                newNode['mesh'] = mesh
                newNode['physics_type'] = str(thingy['physics_type'].nodeValue)
                if thingy.get('collisionPrim'):
                    newNode['collisionPrim'] = str(thingy['collisionPrim'].nodeValue)
                self.nodes.append(newNode)
    
        self.newNode = newNode
        
    def findNodes (self,root, name):
        out=minidom.NodeList()
        if root.hasChildNodes:
            nodes = root.childNodes
            for node in nodes:
                if node.nodeType == Node.ELEMENT_NODE and node.nodeName == name:
                    out.append(node)
        return out

if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))
