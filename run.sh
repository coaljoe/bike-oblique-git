#!/bin/sh

cd `dirname $0`
export LD_LIBRARY_PATH=`pwd`
export PYTHONPATH=`pwd`

if [ "$1" == "-d" ]; then
    shift
    export DUBUG=1
    gdb --args python2 ./py/main.py $*
else
    python2 ./py/main.py $*
fi
